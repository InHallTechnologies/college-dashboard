import React, { createContext, useReducer } from "react";
const Context = createContext();


const reducerFunction = (state, action) => {
    switch(action.type) {
        case "SET_GLOBAL_USER_DATA": {
            return {
                ...state,
                ...action.payload
            }
        }

        case "GET_GLOBAL_USER_DATA": {
            return {
                ...state
            }
        }

        default: {
            return state;
        }
    }
}

export const Provider = ({ children }) => {
    const [globalState, dispatchForGlobalState] = useReducer(reducerFunction, {});
    return(
        <Context.Provider value={[globalState, dispatchForGlobalState]} >
            {children}
        </Context.Provider>
    )
}

export default Context;
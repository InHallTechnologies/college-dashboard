import "./App.css";
import { Routes, Route } from "react-router-dom";
import LoginPage from "./pages/Login/login.component";
import SignupPage from "./pages/Signup/signup.component";
import HomePage from "./pages/home/home.component";
import Context, { Provider } from "./context/appContext";
import { useContext, useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";
import { firebaseAuth, firebaseDatabase } from "./backend/firebase-handler";
import EditJobPage from "./pages/edit-job/edit-job.component";
import { ref, get, push, set } from 'firebase/database';
import AddEditStudentPage from "./pages/add-edit-student/addEditStudent.component";
import CompanyProfileContainer from "./pages/add-edit-company/addEditCompany.component";
import UploadStudentSpreadsheet from "./pages/upload-student-spreadsheet/upload-student-spreadsheet.component";
import Applications from "./pages/applications/applications.component";
import StudentDetail from "./pages/student-detail/student-detail.component";

function App() {

    const [loading, setLoading] = useState(true);
    const [globalState, dispatchForGlobalState] = useContext(Context);
  
    useEffect(() => {
        // firebaseAuth.signOut()
        firebaseAuth.onAuthStateChanged( async (user) => {
            if (user){
                const userReference = ref(firebaseDatabase, `COLLEGE_ARCHIVE/${user.uid}`);
                const snapshot = await get(userReference);
                if (snapshot.exists()){
                    dispatchForGlobalState({type:'SET_GLOBAL_USER_DATA', payload: snapshot.val()})
                    var tempKey = push(ref(firebaseDatabase, "LOG/COLLEGE/")).key
                    var date = new Date().getDate(); 
                    var month = new Date().getMonth() + 1; 
                    var year = new Date().getFullYear(); 
                    var hours = new Date().getHours(); 
                    var min = new Date().getMinutes();
                    var sec = new Date().getSeconds(); 
                    var logDate = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
                    var tempObj = {key:tempKey, date:logDate, id:snapshot.child("uid").val(), name:snapshot.child("collegeName").val(), url:snapshot.child("companyLogo").val(), type:"LOGIN"}
                    await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/log/"+tempKey), tempObj)
                    await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/name"), tempObj.name)
                }
                setLoading(false);
            }else {
                setLoading(false)
            }
        })
    }, [])

    if (loading) {
        return(
            <div className="loader-container">
                <Spinner animation="border" />
            </div>
        )
    }
    

    return (
        <>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/signup" element={<SignupPage />} />
                <Route path='/student-details/:studentId' element={<AddEditStudentPage />} />
                <Route path='/company-details/:companyId' element={<CompanyProfileContainer />} />
                <Route path="/job-list" element={<EditJobPage />} />
                <Route path="/upload-student-spreadsheet" element={<UploadStudentSpreadsheet />} />
                <Route path="/applications" element={<Applications/>} />
                <Route path="/student-detail" element={<StudentDetail/>} />
            </Routes>
        </>
    );
}

export default () => {
    return (
        <Provider>
            <App />
        </Provider>
    )
};

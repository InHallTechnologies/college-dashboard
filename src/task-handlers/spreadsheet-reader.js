import * as XLSX from 'xlsx';
import sampleStudent from "../entities/sampleStudent";

export const readSpreadSheet = (file, callback) => {
    const reader = new FileReader();
    const finalObject = [];
    reader.onload = evt => {
        const bstr = evt.target.result;
        const wb = XLSX.read(bstr, {type:'binary'});
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        const data = XLSX.utils.sheet_to_json(ws, {header:1});

        if (data[0].length === 5){
            for (let i = 1 ; i <= (data.length - 1) ; i++){
                const [usn, name, phoneNumber, emailId, semester] = data[i];
                if (usn && name && phoneNumber && emailId && semester){
                    finalObject.push({...sampleStudent, usn, name, phoneNumber, emailId, semester});
                }else {

                    callback('error')
                }
            }
            callback(finalObject)
        }else {
            console.log("i am here", data[0])
            callback("error")
        }

    }
    reader.readAsBinaryString(file);
}


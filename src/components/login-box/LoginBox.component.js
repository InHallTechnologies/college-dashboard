import React, { useState } from "react";
import './LoginBox.style.scss';
import { Button, Spinner } from "react-bootstrap";
import validator from 'validator';
import { sendPasswordResetEmail, signInWithEmailAndPassword } from 'firebase/auth';
import { useNavigate } from "react-router-dom";
import { firebaseAuth } from "../../backend/firebase-handler";


const LoginBox = () => {

    const navigate = useNavigate();

    const [credentials, setCredentials] = useState({
        email:'',
        password:''
    })

    const [errorSet, setErrorSet] = useState({
        emailError:'',
        passwordError:""
    })
   
    const [buttonLoading, setButtonLoading] = useState(false);


    const handleSubmit = async (event) => {
        console.log(credentials)
        if (!validator.isEmail(credentials.email)){
            setErrorSet(errors => ({...errors, emailError:"Please enter a valid email id"}));
            return;
        }

        if (credentials.password.length < 8) {
            setErrorSet(errors => ({...errors, passwordError:"Minimum password width should be 8"}));
            return;
        }
        try {
            setButtonLoading(true);
            await signInWithEmailAndPassword( firebaseAuth ,credentials.email, credentials.password)
            navigate('/');
        }catch(err) {
            setButtonLoading(false)
            setErrorSet(errors => ({...errors, passwordError:"Invalid Email-ID or password"}));
        }
    }

    const handleChange = (event) => {
        const { value, name } = event.target;
        setErrorSet({emailError:'', passwordError:''})
        setCredentials({
            ...credentials,
            [name]:value
        })
    }

    const handleForgot = async () => {
        if (credentials.email === "") {
            setErrorSet(errors => ({...errors, emailError:"Please enter your email-id first"}));
            return;
        } else {
            try {
                await sendPasswordResetEmail(firebaseAuth, credentials.email)
                alert("We have sent a reset-password link to your email-Id. Please click on the link to reset your password.")
            } catch (e) {
                alert("Something went wrong. Please confirm your email-Id or try again later.")
            }
        }
    }

    return(
        <div className="login-box-container" >
            <div className="input-container">
                <label className="label" >Email Id</label>
                <input name="email" onChange={handleChange} required className="input" id='emailId' placeholder="someone@example.com" type={'email'} />       
                {
                    errorSet.emailError
                    ?
                    <span className="error-label" >{errorSet.emailError}</span>
                    :
                    null
                }
            </div>

            <div className="input-container">
                <label className="label" >Password</label>
                <input name="password" onChange={handleChange} required className="input password" id='emailId' placeholder="**********" type={'password'} />       
                {
                    errorSet.passwordError
                    ?
                    <span className="error-label" >{errorSet.passwordError}</span>
                    :
                    null
                }
            </div>

            <div className="button-container">
                <Button disabled={buttonLoading} onClick={handleSubmit} className="login-button" type="submit" variant="primary">
                    {
                        buttonLoading
                        ?
                        <Spinner  animation="border" size="sm" color="white" />
                        :
                        <span>Login</span>
                    }
                    
                </Button>
                <span className="forgot-password" onClick={handleForgot} >Forgot Password ?</span>
            </div>

        </div>
    )
}

export default LoginBox;
import React from 'react';
import './StudentsListArch.style.css';
import {Button, Card} from "react-bootstrap";
import {MdAlternateEmail, HiOutlinePhone} from "react-icons/all";
import { useNavigate } from 'react-router-dom';

const StudentsListArch = ({ student, handleClick, hideViewDetail }) => {
    const { name, usn, phoneNumber, emailId, uid, picture } = student;
    const navigate = useNavigate()

    return (
       
        <tr>
            <td>
                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                    <img src={picture===""?"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fstudent.png?alt=media&token=59f398c7-546c-445d-8525-0294a6e0d053":picture} alt={name} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                    <p>{name}</p>
                </div>
            </td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>{usn}</div></td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>{emailId}</div></td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>{phoneNumber}</div></td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>
                <Button className="upgrade-button" variant="primary" onClick={()=>{navigate("/student-detail", {state:{item:student, type:"RESUME"}})}} >Resume</Button>
            </div></td>
        </tr>
    )
}

export  default StudentsListArch;
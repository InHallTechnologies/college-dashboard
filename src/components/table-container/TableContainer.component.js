import React, { useEffect, useState } from "react";
import { Button, Table, Spinner } from "react-bootstrap";
import TableItems from "../table-items/TableItems.component";
import "./TableContainer.style.scss";


const TableContainer = ({ tableItems, loading }) => {
    const [screenSize, setScreenSize] = useState(window.innerWidth);
    useEffect(() => {


        window.addEventListener('resize', handleScreenSize);
        return () => {
            window.removeEventListener('resize', handleScreenSize);
        }

    }, [])

    const handleScreenSize = () => {
        setScreenSize(window.innerWidth);
    }



    return (
        <div className="table-content">
            <h2 className="section-title">
                Placement Statistics
            </h2>

            {
                loading
                    ?
                    <div className='loading-container' >
                        <Spinner animation="border" size="sm" />
                    </div>
                    :
                    null
            }

            {
                !loading && tableItems.length !== 0
                    ?
                    <Table style={{ marginTop: "10px" }} responsive="sm" hover >
                        <thead>
                            <tr>
                                <th>Company</th>
                                <th>Students Placed</th>
                                <th>Average Package</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                tableItems.map((item) => <TableItems item={item} screenSize={screenSize} />)
                            }
                        </tbody>
                    </Table>
                    :
                    <div className="no-vacancy-container">
                        <span className="no-vacancy-label">No details</span>
                    </div>
            }

            {
                !loading && tableItems.length !== 0
                    ?
                    <Button variant="link">View All</Button>
                    :
                    null
            }
        </div>
    );
};

export default TableContainer;

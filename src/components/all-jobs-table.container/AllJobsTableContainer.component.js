import React, { useEffect, useState } from "react";
import { Table, Spinner } from "react-bootstrap";
import TableItems from "../table-items/TableItems.component";
import "./AllJobsTableContainer.style.scss";
import { firebaseAuth, firebaseDatabase } from "../../backend/firebase-handler";
import { ref, get } from 'firebase/database';

const AllJobsTableContainer = () => {
    const [screenSize, setScreenSize] = useState(window.innerWidth);
    const [loading, setLoading] = useState(true);
    const [tableItems, setTableItems] = useState([]);
    const firebaseUser = firebaseAuth.currentUser;


    useEffect(() => {
        (async () => {
            const jobListReference = ref(firebaseDatabase, `COMPANY_WISE_VACANCY/${firebaseUser.uid}`);
            const snapshot = await get(jobListReference)
            if (snapshot.exists()){
                const data = [];
                for (const key in snapshot.val()) {
                    const job = snapshot.child(key).val();
                    data.push(job);
                }
                setTableItems(data);
                setLoading(false);
            }else {
                setLoading(false)
            }
        })()
        
        window.addEventListener('resize', handleScreenSize);
        return () => {
            window.removeEventListener('resize', handleScreenSize);
        }

    }, [])

    const handleScreenSize = () => {
        setScreenSize(window.innerWidth);
    }

   

    return (
        <div className="table-content">
            <h2 className="section-title">
                All Jobs
            </h2>

            {
                loading
                ?
                <div className='loading-container' >
                    <Spinner animation="border" size="sm" />
                </div>
                :
                null        
            }
            
            {
                !loading && tableItems.length !== 0
                ?
                <Table style={{ marginTop: "10px" }} responsive="sm" hover >
                    <thead>
                        <tr>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>
                            {
                                screenSize >= 550
                                ?
                                <th>Actions</th>
                                :
                                null
                            }
                            
                        </tr>
                    </thead>
                    <tbody>
                        {
                            tableItems.map((item) => <TableItems item={item} screenSize={screenSize} />)
                        }
                    </tbody>
                </Table>
                :
                <div className="no-vacancy-container">
                    <span className="no-vacancy-label">No added vacancy</span>
                </div>
            }
        </div>
    );
};

export default AllJobsTableContainer;

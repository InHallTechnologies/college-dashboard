import React, {useEffect, useState} from 'react';
import './ManageStudents.styles.scss';
import ProfileContainer from "../profile-container/ProfileContainer.component";
import {Dropdown, Spinner, Form, Table, Button} from "react-bootstrap";
import StudentsListArch from "../students-list-arch/StudentsListArch.component";
import SearchBar from "../search-bar/SearchBar.component";
import {useNavigate} from "react-router-dom";
import {child, get, ref} from "firebase/database";
import {firebaseAuth, firebaseDatabase} from "../../backend/firebase-handler";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

const ManageStudents = () => {
    const [studentList, setStudentList] = useState([]);
    const [branchList, setBranchList] = useState([])
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();
    const firebaseUser = firebaseAuth.currentUser;
    const [startDate, setStartDate] = useState(new Date());
    const [searchQuery, setSearchQuery] = useState("");
    const [selectedDepartment, setSelectedDepartment] = useState("");
    const [applyingFilter, setApplyingFilter] = useState(false)

    useEffect(() => {
        const studentListReference = ref(firebaseDatabase,`COLLEGE_WISE_STUDENTS/${firebaseUser.uid}`);
        get(studentListReference).then(snapshot => {
            if (snapshot.exists()){
                const data = [];
                for (const branch in snapshot.val()) {
                    data.push(branch);
                }
                setBranchList(data);
                setLoading(false)
            }
            setLoading(false);
        })
    }, [])

    const handleNewStudent = (id) => {
        navigate(`student-details/${id}`)
    }

    const handleSpreadsheetUpload = () => { 
        navigate('/upload-student-spreadsheet')
    }

    const handleFilter = async () => {
        setApplyingFilter(true)
        let tempStudents = []
        await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+firebaseUser.uid+"/"+selectedDepartment)).then((snap)=>{
            if (snap.exists()) {
                for  (const key in snap.val()) {
                    if (snap.child(key).child("yearOfStudy").val() === startDate.getFullYear().toString()) {
                        tempStudents.push(snap.child(key).val())
                    }
                }
                setStudentList(tempStudents)
                setApplyingFilter(false)
            } else {
                setApplyingFilter(false)
            }
        })
    }

    const searchHandler = async () => {
        setApplyingFilter(true)
        let tempStudents = []
        await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+firebaseUser.uid)).then( async (snap)=>{
            if (snap.exists()) {
                for (const department in snap.val()) {
                    await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+firebaseUser.uid+"/"+department)).then((snap2) => {
                        for (const key in snap2.val()) {
                            console.log(key, searchQuery)
                            if (snap2.child(key).child("usn").val().toLowerCase() === searchQuery.toLowerCase()) {
                                tempStudents.push(snap2.child(key).val())
                            }
                        }
                    })
                }
                setStudentList(tempStudents)
                setApplyingFilter(false)
                console.log(tempStudents[0], " aaa")
            } else {
                setApplyingFilter(false)
            }
        })
    }

    return(
        <div className='manage-students-content-container'>
            <ProfileContainer />
            <div className={'manage-students-content'}>
                <SearchBar placeholder={'Search by USN'} onSearchEntered={searchHandler} searchWord={searchQuery} setSearchWord={setSearchQuery} />
                <div className={'students-list-container'}>
                    <div className='title-button-container'>
                        <p className={'section-title'} >Manage Students</p>

                        <Dropdown className={'add-students-drop-down'} >
                            <Dropdown.Toggle variant={'primary'} id="dropdown-basic">
                                Add Students
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Item onClick={_ => handleNewStudent('add-new-student')} >Add Student</Dropdown.Item>
                                <Dropdown.Item onClick={handleSpreadsheetUpload}>Upload Spreadsheet</Dropdown.Item>
                                <Dropdown.Item onClick={()=>{window.open("https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/OccuHire%20Student%20Detail%20Sample%20Spreadsheet.xlsx?alt=media&token=a61b06c3-0ff3-4ee2-a313-7f53f6fd3afb", "_blank")}}>Download Sample Spreadsheet</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    
                    <div className="filter-container2">
                        <Form.Group className="year-picker2" controlId="exampleForm.ControlInput1">
                            <Form.Label>Graduation Year</Form.Label>
                            <DatePicker
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                                showYearPicker
                                dateFormat="yyyy"
                                className='picker-input'
                            />
                        </Form.Group>
                        <Form.Group className="department-picker2" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Department</Form.Label>
                            <Form.Select value={selectedDepartment} onChange={event => setSelectedDepartment(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select department</option>
                                {
                                    branchList.map(item => <option key={item} >{item}</option>)
                                }
                            </Form.Select>
                        </Form.Group>
                        <Button disabled={applyingFilter} className="search-button2" variant="primary" onClick={handleFilter}>Apply filter</Button>
                    </div>  

                    {
                        loading
                        ?
                        <div className={'loading-container'}>
                            <Spinner animation="border" />
                        </div>
                        :
                        studentList.length === 0
                        ?
                        <div className={'loading-container'}>
                            <span className={'empty-label'}>No students here</span>
                        </div>
                        :
                        <Table hover className="table" responsive="sm" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>USN</th>
                                <th>Email-Id</th>
                                <th>Phone Number</th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody className="table-body">
                            {
                                studentList.map(item => {return(
                                    <tr>
                                        <td>
                                            <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                <img src={item.picture===""?"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fstudent.png?alt=media&token=59f398c7-546c-445d-8525-0294a6e0d053":item.picture} alt={item.name} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                <p>{item.name}</p>
                                            </div>
                                        </td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.usn}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.emailId}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.phoneNumber}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>
                                            <Button className="upgrade-button" variant="primary" onClick={()=>{navigate("/student-detail", {state:{item:item, type:"RESUME"}})}} >Resume</Button>
                                        </div></td>
                                    </tr>
                                )})
                            }
                            </tbody>
                        </Table>
                    }
                </div>
            </div>
        </div>
    )
}

export default ManageStudents;

 {/* )} <StudentsListArch key={item.uid} student={item} handleClick={handleNewStudent}  />) */}
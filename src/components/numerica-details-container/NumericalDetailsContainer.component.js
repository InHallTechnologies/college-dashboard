import React, { useContext, useEffect, useState } from 'react';
import './NumericalDetailsContainer.style.scss';
import { Form, Spinner, Button } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import { get, child, ref } from 'firebase/database'
import { firebaseDatabase } from '../../backend/firebase-handler';
import Context from '../../context/appContext';


const NumericalDetailsContainer = ({ loading , tableItems }) => {
    const [startDate, setStartDate] = useState(new Date());
    const [selectedDepartment, setSelectedDepartment] = useState("All Branches");
    const [noOfStudents, setNoOfStudents] = useState("-")
    const [placed, setPlaced] = useState("-")
    const [applyingFilter, setApplyingFilter] = useState(false)
    const [globalState, dispatchForGlobalState] = useContext(Context)
    const [departmentList, setDepartmentList] = useState([])
    const [highest, setHighest] = useState("")
    const [average, setAverage] = useState("")

    useEffect(()=>{
        let tempStudents = 0
        let placedTemp = 0
        
        let tempDepart = ["All Branches"]
        get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+globalState.uid)).then((snap)=>{
            if (snap.exists()) {
                let highest = "0"
                let averageArray = []
                for  (const key in snap.val()) {
                    tempDepart.push(key)
                    for(const key2 in snap.child(key).val()) {
                        if (snap.child(key).child(key2).child("yearOfStudy").val() === "2022") {
                            tempStudents++
                            if (snap.child(key).child(key2).child("placed").exists()) {
                                let placedArray = snap.child(key).child(key2).child("placed").val()
                                let localHighest = "0"
                                for (const index in placedArray) {
                                    if (parseInt(placedArray[index].package) > parseInt(localHighest)) {
                                        localHighest = placedArray[index].package

                                    }
                                }
                                if (parseInt(localHighest) > parseInt(highest)) {
                                    highest = localHighest
                                }
                                averageArray.push(localHighest)
                                placedTemp++
                            } 
                        }
                    }
                    
                }
                setDepartmentList(tempDepart)
                setNoOfStudents(tempStudents)
                setPlaced(placedTemp)
                setApplyingFilter(false)
                setHighest((parseFloat(highest)/100000).toFixed(1))
                let tempSum = 0
                if  (averageArray.length === 0) {
                    setAverage("0")
                } else {
                    for (const index in averageArray) {
                        tempSum += parseInt(averageArray[index])
                    }
                    setAverage(parseFloat((tempSum/averageArray.length)/100000).toFixed(1))
                }
            } else {
                setNoOfStudents(tempStudents)
                setPlaced(placedTemp)
                setApplyingFilter(false)
            }
        })
    }, [])

    if (loading) {
        return (
            <div className='numerical-details-content' style={{padding:'40px 20px'}} >
                <Spinner animation="border" size='sm' />
            </div>
        )
    }

    const handleFilter = async () => {
        setApplyingFilter(true)
        let highest = "0"
        let averageArray = []
        if (selectedDepartment === "All Branches") {
            let tempStudents = 0
            let placedTemp = 0
            await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+globalState.uid)).then((snap)=>{
                if (snap.exists()) {
                    for  (const key in snap.val()) {
                        for (const key2 in snap.child(key).val()) {
                            if (snap.child(key).child(key2).child("yearOfStudy").val() === startDate.getFullYear().toString()) {
                                tempStudents++
                                if (snap.child(key).child(key2).child("placed").exists()) {
                                    let placedArray = snap.child(key).child(key2).child("placed").val()
                                    let localHighest = "0"
                                    for (const index in placedArray) {
                                        if (parseInt(placedArray[index].package) > parseInt(localHighest)) {
                                            localHighest = placedArray[index].package

                                        }
                                    }
                                    if (parseInt(localHighest) > parseInt(highest)) {
                                        highest = localHighest
                                    }
                                    averageArray.push(localHighest)
                                    placedTemp++
                                } 
                            }
                        }
                    }
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setApplyingFilter(false)
                    setHighest((parseFloat(highest)/100000).toFixed(1))
                    let tempSum = 0
                    if  (averageArray.length === 0) {
                        setAverage("0")
                    } else {
                        for (const index in averageArray) {
                            tempSum += parseInt(averageArray[index])
                        }
                        setAverage(parseFloat((tempSum/averageArray.length)/100000).toFixed(1))
                    }
                } else {
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setApplyingFilter(false)
                }
            })
        } else {
            let tempStudents = 0
            let placedTemp = 0
            
            await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+globalState.uid+"/"+selectedDepartment)).then((snap)=>{
                if (snap.exists()) {
                    for  (const key in snap.val()) {
                        if (snap.child(key).child("yearOfStudy").val() === startDate.getFullYear().toString()) {
                            tempStudents++
                            if (snap.child(key).child("placed").exists()) {
                                let placedArray = snap.child(key).child("placed").val()
                                let localHighest = "0"
                                for (const index in placedArray) {
                                    if (parseInt(placedArray[index].package) > parseInt(localHighest)) {
                                        localHighest = placedArray[index].package

                                    }
                                }
                                if (parseInt(localHighest) > parseInt(highest)) {
                                    highest = localHighest
                                }
                                averageArray.push(localHighest)
                                placedTemp++
                            } 
                        }
                    }
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setApplyingFilter(false)
                    setHighest((parseFloat(highest)/100000).toFixed(1))
                    let tempSum = 0
                    if  (averageArray.length === 0) {
                        setAverage("0")
                    } else {
                        for (const index in averageArray) {
                            tempSum += parseInt(averageArray[index])
                        }
                        setAverage(parseFloat((tempSum/averageArray.length)/100000).toFixed(1))
                    }
                } else {
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setApplyingFilter(false)
                }
            })
        }
    }

    return(
        <div className='numerical-details-main-container'>
            <div className="stats-filter-container">
                <div className="filter-container">
                    <Form.Group className="mb-3 year-picker" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Graduation Year</Form.Label>
                        <DatePicker
                            selected={startDate}
                            onChange={(date) => setStartDate(date)}
                            showYearPicker
                            dateFormat="yyyy"
                            className='picker-input'
                        />
                    </Form.Group>
                    <Form.Group className="mb-3 department-picker" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Department</Form.Label>
                        <Form.Select value={selectedDepartment} onChange={event => setSelectedDepartment(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                            <option>Select department</option>
                            {
                                departmentList.map(item => <option key={item} >{item}</option>)
                            }
                        </Form.Select>
                    </Form.Group>
                    <Button disabled={applyingFilter} className="search-button" variant="primary" onClick={handleFilter}>Apply filter</Button>
                </div>
                <div className="stats-container">
                    <div className="tag-data-container">
                        <p className="tag">No of Students</p>
                        <p className="data">{noOfStudents.toString()}</p>
                    </div>
                    {/* <div className="stat-bar"></div> */}
                    <div className="tag-data-container">
                        <p className="tag">Placed Students</p>
                        <p className="data">{placed.toString()}</p>
                    </div>
                    {/* <div className="stat-bar"></div> */}
                    <div className="tag-data-container">
                        <p className="tag">Average Package</p>
                        <p className="data">{average}L</p>
                    </div>
                    <div className="tag-data-container">
                        <p className="tag">Highest Package</p>
                        <p className="data">{highest}L</p>
                    </div>
                </div>
            </div>                
        </div>
    )
}

export default NumericalDetailsContainer;
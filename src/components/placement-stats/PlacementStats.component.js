import React, {useContext, useEffect, useState} from 'react';
import './PlacementStats.style.scss';
import ProfileContainer from "../profile-container/ProfileContainer.component";
import {Button, Form, Spinner, Table} from "react-bootstrap";
import { get, ref, child } from 'firebase/database'
import { firebaseDatabase } from '../../backend/firebase-handler';
import Context from '../../context/appContext';
import DatePicker from 'react-datepicker';
import { useNavigate } from 'react-router-dom';

const PlacementStats = () => {
    const [loading, setLoading] = useState(true);
    const [studentList, setStudentList] = useState([])
    const [screenSize, setScreenSize] = useState(window.innerWidth);
    const [globalState, dispatchForGlobalState] = useContext(Context)
    const [startDate, setStartDate] = useState(new Date());
    const [selectedDepartment, setSelectedDepartment] = useState("");
    const [departmentList, setDepartmentList] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        window.addEventListener('resize', handleScreenSize);

        var temp = []
        get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+globalState.uid)).then((snapShot)=>{
            if (snapShot.exists()) {
                for  (const key in snapShot.val()) {
                    temp.push(key)
                }
                setDepartmentList(temp)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
        return () => {
            window.removeEventListener('resize', handleScreenSize);
        }

    }, [])

    const handleFilter = async () => {
        setLoading(true)
        var temp = []
        
        await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+globalState.uid+"/"+selectedDepartment)).then((snapShot)=>{
            if (snapShot.exists()) {
                for  (const key in snapShot.val()) {
                    if (snapShot.child(key).child("yearOfStudy").val() === startDate.getFullYear().toString()) {
                        temp.push(snapShot.child(key).val())
                    }
                }
                setStudentList(temp.reverse())
                setLoading(false)
            } else {
                setStudentList(temp)
                setLoading(false)
            }
        })
    }

    const handleScreenSize = () => {
        setScreenSize(window.innerWidth);
    }

    return(
        <div className={'placement-stats-container'} >
            <ProfileContainer />
            <div className={'content'} >
                {/* <SearchBar /> */}
                <div className="table-content">
                    <h2 className="section-title">
                        Placement Statistics
                    </h2>

                    <div className="filter-container">
                        <Form.Group className="mb-3 year-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Graduation Year</Form.Label>
                            <DatePicker
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                                showYearPicker
                                dateFormat="yyyy"
                                className='picker-input'
                            />
                        </Form.Group>

                        <Form.Group className="mb-3 department-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Department</Form.Label>
                            <Form.Select value={selectedDepartment} onChange={event => setSelectedDepartment(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select department</option>
                                {
                                    departmentList.map(item => <option key={item} >{item}</option>)
                                }
                            </Form.Select>
                        </Form.Group>
                        <Button className="search-button" variant="primary" onClick={handleFilter}>Apply filter</Button>
                    </div>

                    {
                        loading
                        ?
                        <div className='loading-container' >
                            <Spinner animation="border" size="sm" />
                        </div>
                        :
                        studentList.length === 0
                        ?
                        <div className="no-vacancy-container">
                            <span className="no-vacancy-label">No students added here!</span>
                        </div>
                        :
                        <Table style={{ marginTop: "10px" }} className="table" responsive="sm" hover >
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>USN</th>
                                    <th>Semester</th>
                                    <th>No. of offers</th>
                                </tr>
                            </thead>
                            <tbody className="table-body">
                                {
                                    studentList.map((item, index) => {return(
                                        <tr onClick={()=>{navigate("/student-detail", {state:{item:item, type:"OFFERS"}})}}>
                                            <td>
                                                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                    <img src={item.picture?item.picture:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                    <p>{item.name}</p>
                                                </div>
                                            </td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.usn}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.semester}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placed?item.placed.length:"0"}</div></td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }
                </div>
            </div>
        </div>
    )
}

export  default  PlacementStats;
import React, { useContext, useEffect, useState } from 'react';
import ProfileContainer from '../profile-container/ProfileContainer.component';
import './AddPostContentContainer.style.scss';
import { Form, Button, Spinner } from 'react-bootstrap';
import sampleJobs from '../../entities/sampleJobs';
import { firebaseAuth, firebaseDatabase } from '../../backend/firebase-handler';
import { ref, set, push, get, child } from 'firebase/database';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Context from '../../context/appContext';
import Multiselect from 'multiselect-react-dropdown'


const AddPostContentContainer = () => {
    const [sampleJobPosting, setSampleJobPosting] = useState({...sampleJobs});
    const [globalState, dispatchForGlobalState] = useContext(Context);
    const [loading, setLoading] = useState(false);
    const firebaseUser = firebaseAuth.currentUser;
    const [yearList, setYearList] = useState([])
    const [list, setList] = useState([])
    const [examList, setExamList] = useState([])

    useEffect(() => {
        let array = []
        var count = 0
        get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+globalState.uid)).then((snap)=>{
            if(snap.exists()) {
                for (const key in snap.val()) {
                    count++
                    let temp2 = {name:"", id:""}
                    temp2.name = key
                    temp2.id = count.toString()
                    console.log(temp2)
                    array.push(temp2)
                }
                console.log(array)
                setList(array)
            }
        })
        let yearArray = []
        const year = new Date().getFullYear()
        for (let i=year-5 ; i<=year+5 ; i++) {
            let temp = {name:"", id:""}
            temp.name = i.toString()
            temp.id = i.toString()
            yearArray.push(temp)
        }
        setYearList(yearArray)  
    }, [])

    const handleTextChange = (event) => {
        const { name, value } = event.target;
        if (name === 'startPackage' || name === 'endPackage' || name === 'cutoffCGPA' || name === 'totalOpenings' || name === 'min10' || name === 'min12'){
            setSampleJobPosting({...sampleJobPosting, [name]: value.replace(/[^0-9.]/g, '')});
        }else {
            setSampleJobPosting({...sampleJobPosting, [name]: value});
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            setLoading(true);           
            let jobListReference = ref(firebaseDatabase, `COLLEGE_WISE_JOBS/${firebaseUser.uid}`);
            const jobReferenceId = push(jobListReference).key
            
            jobListReference = ref(firebaseDatabase, `COLLEGE_WISE_JOBS/${firebaseUser.uid}/${jobReferenceId}`);
            await set(jobListReference, {
                ...sampleJobPosting, 
                companyUID: firebaseUser.uid, 
                jobId:jobReferenceId,
                companyLogo: globalState.companyLogo? globalState.companyLogo: '',
                companyName: globalState.companyName?globalState.companyName:'' 
            })

            setSampleJobPosting({...sampleJobs});
            toast.success('New job vacancy posted successfully', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }catch(err) {
            setLoading(false);
            toast.error('Something went wrong', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            console.log(err);
        }
    }

    return(
        <div className='add-post-content-container'>
            <ProfileContainer />
            <div className='add-post-form-container'>
                <h2 className='section-title'>Add New Job</h2>
                <div className='main-form-container'>
                    <Form className='main-form' onSubmit={handleSubmit} >
                        <Form.Group  className="mb-3 first-half">
                            <Form.Label className='text-label'>Role</Form.Label>
                            <Form.Control required name='role' value={sampleJobPosting.role} onChange={handleTextChange}  size='lg' className='text-input'  />
                        </Form.Group>

                        <Form.Group className="mb-3 second-half">
                            <Form.Label className='text-label'>Location</Form.Label>
                            <Form.Control required name='location' value={sampleJobPosting.location} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>

                        <Form.Group className="mb-3 end-to-end">
                            <Form.Label className='text-label'>Description</Form.Label>
                            <Form.Control required name='jobDescription' value={sampleJobPosting.jobDescription} onChange={handleTextChange} className='text-area text-input' as="textarea"  />
                        </Form.Group>

                        <Form.Group className="mb-3 first-half">
                            <Form.Label className='text-label' >Start Package</Form.Label>
                            <Form.Control required name='startPackage'  value={sampleJobPosting.startPackage} onChange={handleTextChange} size='lg' className='text-input'  />
                        </Form.Group>

                        <Form.Group className="mb-3 second-half">
                            <Form.Label className='text-label'>End Package</Form.Label>
                            <Form.Control required name='endPackage'  value={sampleJobPosting.endPackage} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>

                        <Form.Group className="mb-3 first-half">
                            <Form.Label className='text-label'>Total Opening</Form.Label>
                            <Form.Control required name='totalOpenings' value={sampleJobPosting.totalOpenings} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>

                        <Form.Group className="mb-3 second-half">
                            <Form.Label className='text-label'>Cutoff CGPA</Form.Label>
                            <Form.Control required name='cutoffCGPA' value={sampleJobPosting.cutoffCGPA} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>

                        <Form.Group className="mb-3 first-half">
                            <Form.Label className='text-label'>Min 10th Percentage</Form.Label>
                            <Form.Control required name='min10' value={sampleJobPosting.min10} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>

                        <Form.Group className="mb-3 second-half">
                            <Form.Label className='text-label'>Min 12th Percentage</Form.Label>
                            <Form.Control required name='min12' value={sampleJobPosting.min12} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>


                        <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">Select allowed branches</Form.Label>
                            {
                                !sampleJobPosting.allBranches
                                &&
                                <Multiselect className='field' options={list} selectedValues={sampleJobPosting.allowedBranches} displayValue="name" onSelect={(value)=>{setSampleJobPosting({...sampleJobPosting, allowedBranches:value})}} onRemove={(value)=>{setSampleJobPosting({...sampleJobPosting, allowedBranches:value})}}/>
                            }
                            <div className='ete-field'><Form.Check className="tag" style={{marginTop:5}}  inline label="Allow all branches" name="group1" type={"checkbox"} checked={sampleJobPosting.allBranches} id={"allowAll"} onChange={(event)=>{setSampleJobPosting({...sampleJobPosting, allBranches:!sampleJobPosting.allBranches})}} /></div>
                        </Form.Group>
                        
                        <Form.Group className="mb-3 end-to-end">
                            <Form.Label className='text-label'>Preferred Skills</Form.Label>
                            <Form.Control required name='preferredSkills' value={sampleJobPosting.preferredSkills} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>

                        <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">Select year of graduation</Form.Label>
                            <Multiselect className='field' options={yearList} selectedValues={sampleJobPosting.graduationYear} displayValue="name" onSelect={(value)=>{setSampleJobPosting({...sampleJobPosting, graduationYear:value})}} onRemove={(value)=>{setSampleJobPosting({...sampleJobPosting, graduationYear:value})}}/>
                        </Form.Group>

                        <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">Registration Link (optional)</Form.Label>
                            <Form.Control className="field" value={sampleJobPosting.registrationLink}  onChange={(event)=>{setSampleJobPosting({...sampleJobPosting, registrationLink:event.target.value})}} />
                        </Form.Group>

                        <Form.Group className="mb-3 end-to-end">
                            <Form.Label className='text-label' >Last Date to Apply</Form.Label>
                            <Form.Control required name='lastDateToApply' value={sampleJobPosting.lastDateToApply} onChange={handleTextChange} size='lg' type='date' className='text-input' />
                        </Form.Group>

                        <Form.Group className="mb-3 end-to-end">
                            <Form.Label className='text-label'>Service Agreement (If any) </Form.Label>
                            <Form.Control required name='serviceAgreement' value={sampleJobPosting.serviceAgreement} onChange={handleTextChange} size='lg' className='text-input' />
                        </Form.Group>

                        
                        
                        
                        <Button disabled={loading} variant="primary" type="submit" className="end-to-end submit-button" >
                            {
                                loading
                                ?
                                <Spinner animation="border" size='sm'/>
                                :
                                <span>Submit</span>
                            }
                        </Button>
                    </Form>
                </div>
            </div>
            <ToastContainer />
        </div>
    )
}

export default AddPostContentContainer;
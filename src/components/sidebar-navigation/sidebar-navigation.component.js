import React, { useEffect, useState } from 'react';
import { MdSpaceDashboard, MdAddBox } from 'react-icons/md';
import { FaUserGraduate, FaListUl } from 'react-icons/fa';
import { GiBallPyramid } from 'react-icons/gi';
import { ImUserTie } from 'react-icons/im';
import { RiSettings5Fill } from 'react-icons/ri';
import { MdOutlineAddBox } from 'react-icons/md';
import { AiFillCode } from 'react-icons/ai'
import { IoStatsChart } from 'react-icons/io5'
import { HiOutlineDocumentReport } from 'react-icons/hi';
import './sidebar-navigation.style.scss';

import { useLocation, useNavigate } from 'react-router-dom';

const SidebarNavigation = ({ onChange }) => {

    const location = useLocation();
    const navigation = useNavigate()
    const [selectedTab, setSelectedTab] = useState("Dashboard");

    useEffect(() => {
        if (location.search){
            const params = new URLSearchParams(location.search);
            setSelectedTab(params.get('tab'));
            if (onChange){
                onChange(params.get('tab'), false)
            }
        }
        
    } , [])

    const handleChange = option => {
        setSelectedTab(option)
        navigation(`/?tab=${option}`)
        if (onChange){
            onChange(option)
        }

    }

    return(
        <div className='sidebar-navigation-container'>
            <div className={selectedTab === "Dashboard"?'selected-option-container':'option-container'} onClick={_ => handleChange("Dashboard")} >
                <MdSpaceDashboard size={20} color={selectedTab === "Dashboard"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Dashboard</h4>
            </div>

            <div className={selectedTab === "ManageStudents"?'selected-option-container':'option-container'} onClick={_ => handleChange("ManageStudents")} >
                <FaUserGraduate size={20} color={selectedTab === "ManageStudents"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Manage Students</h4>
            </div>

            <div className={selectedTab === "PlacementStats"?'selected-option-container':'option-container'} onClick={_ => handleChange("PlacementStats")} >
                <IoStatsChart size={20} color={selectedTab === "PlacementStats"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Placement Stats</h4>
            </div>

            <div className={selectedTab === "ManageJobs"?'selected-option-container':'option-container'} onClick={_ => handleChange("ManageJobs")} >
                <FaListUl size={20} color={selectedTab === "ManageJobs"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Manage Jobs</h4>
            </div>

            <div className={selectedTab === "ManageCompanies"?'selected-option-container':'option-container'} onClick={_ => handleChange("ManageCompanies")} >
                <ImUserTie size={20} color={selectedTab === "ManageCompanies"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Manage Companies</h4>
            </div>

            <div className={selectedTab === "AddNewJob"?'selected-option-container':'option-container'} onClick={_ => handleChange("AddNewJob")} >
                <MdOutlineAddBox size={20} color={selectedTab === "AddNewJob"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Post Job</h4>
            </div>

            
            <div className={selectedTab === "AssessmentResults"?'selected-option-container':'option-container'} onClick={_ => handleChange("AssessmentResults")} >
                <AiFillCode size={18} color={selectedTab === "AssessmentResults"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Assessment Results</h4>
            </div>

            {/* <div className={selectedTab === "GenerateReports"?'selected-option-container':'option-container'} onClick={_ => handleChange("GenerateReports")} >
                <HiOutlineDocumentReport size={20} color={selectedTab === "GenerateReports"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Generate Reports</h4>
            </div> */}

            <div className={selectedTab === "ManageProfile"?'selected-option-container':'option-container'} onClick={_ => handleChange("ManageProfile")} >
                <RiSettings5Fill size={20} color={selectedTab === "ManageProfile"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Manage Profile</h4>
            </div>
        </div>
    )
}
export default  React.memo(SidebarNavigation);
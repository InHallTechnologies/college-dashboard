import React from 'react';
import './CompanyListArch.style.css';
import {Card, Button} from "react-bootstrap";
import {HiOutlinePhone, MdAlternateEmail,FaRegAddressCard} from "react-icons/all";
import hr from '../../assets/hr.png';
import { useNavigate } from 'react-router-dom';

const CompanyListArch = ({ company, handleAddNewCompany, type }) => {
    const { companyName, companyLogo, email, hrContactNumber, hrName, companyAddress, uid } = company;
    const navigate = useNavigate()

    return(
        <Card style={{ width: '100%', cursor:type==="SELECT"?"pointer":"default", paddingTop:10 }} onClick={()=>{if(type==="SELECT"){navigate("/job-list", {state:{company:company, type:"ADD"}})}}} >
            <Card.Img style={{width:180, height:180, margin:"auto"}} variant="Top" src={companyLogo?companyLogo:'https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fbuildings.png?alt=media&token=41b80aa2-3348-4616-8b04-5f155d9e0b55'} />
            <Card.Body>
                <Card.Title title={"Company Name"} >{companyName}</Card.Title>
                <div title={"HR Name"} className={'info-container first-post '} >
                    <img src={hr} className={'hr-logo'} />
                    <Card.Subtitle styles={{margin:"0", marginTop:"2px", marginLeft:"15px", backgroundColor:'pink'}} className="mb-2 text-muted info-value">{hrName}</Card.Subtitle>
                </div>

                <div title={"HR Email Id"} className={'info-container'} >
                    <MdAlternateEmail size={20} color={"#444"} />
                    <Card.Subtitle styles={{margin:"0", marginTop:"2px", marginLeft:"15px", backgroundColor:'pink'}} className="mb-2 text-muted info-value">{email}</Card.Subtitle>
                </div>

                <div title={"HR Phone Number"} className={'info-container'} >
                    <HiOutlinePhone size={20} color={"#444"} />
                    <Card.Subtitle styles={{margin:"0", marginTop:"2px", marginLeft:"15px"}} className="mb-2 text-muted info-value">{hrContactNumber}</Card.Subtitle>
                </div>

                <div title={"HR Phone Number"} className={'info-container last-post'} >
                    <FaRegAddressCard size={20} color={"#444"} />
                    <Card.Subtitle styles={{margin:"0", marginTop:"2px", marginLeft:"15px"}} className="mb-2 text-muted info-value">{companyAddress}</Card.Subtitle>
                </div>
                {
                    type === "VIEW"
                    &&
                    <Card.Link style={{cursor:'pointer'}} onClick={_ => {handleAddNewCompany(uid)}}>View Details</Card.Link>
                }
            </Card.Body>

        </Card>
    )
}
export  default CompanyListArch
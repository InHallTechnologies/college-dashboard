import React, { useState, useEffect, useContext } from 'react';
import './CompanyProfileContainer.style.scss';
import { Form, Button, Image, Spinner, Modal } from 'react-bootstrap';
import { firebaseAuth, firebaseDatabase, firebaseStorage } from '../../backend/firebase-handler';
import { get, ref, set } from 'firebase/database';
import { ToastContainer, toast } from "react-toastify";
import Context from '../../context/appContext';
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import { sendPasswordResetEmail } from 'firebase/auth';
import { FiLogOut } from 'react-icons/fi'

const CompanyProfileContainer = () => {

    const firebaseUser = firebaseAuth.currentUser;
    const [credentials, setCredentials] = useState({
        collegeName: "",
        uid:'',
        emailId:'',
        collegeAddress:'',
        placementOfficerName:'',
        placementOfficerPhoneNumber:'',
        password:'',
        companyLogo:'',
    });
    const [globalState, dispatchForGlobalState] = useContext(Context);
    const [uploadingImage, setUploadingImage] = useState(false);  
    const [uploadPercentage, setUploadPercentage] = useState(0);
    const [modalVisibility, setModalVisibility] = useState(false)


    useEffect(() => {
        const profileRef = ref(firebaseDatabase, `COLLEGE_ARCHIVE/${firebaseUser.uid}`);
        get(profileRef).then(snapshot => {
            if (snapshot.exists()){
                const user = snapshot.val();
                setCredentials(user);
            }
        })
    }, [])

    const handleChange = (event) => {
        const { name, value } = event.target;
        setCredentials({...credentials, [name]: value });
    }

    const handleForgot = async () => {
        setModalVisibility(false)
        try {
            await sendPasswordResetEmail(firebaseAuth, credentials.emailId)
            alert("We have sent a reset-password link to your email-Id. Please click on the link to reset your password and login again.")
            firebaseAuth.signOut()
        } catch (e) {
            alert("Something went wrong. Please confirm your email-Id or try again later.")
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const profileRef = ref(firebaseDatabase, `COLLEGE_ARCHIVE/${firebaseUser.uid}`);
        set(profileRef, {...credentials});
        dispatchForGlobalState({type:'SET_GLOBAL_USER_DATA', payload:{...credentials}});
        toast.success(`Saved Changes`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        
    }

    const handleLogoChange = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            const logoRef = firebaseStorageRef(firebaseStorage, `COLLEGE_LOGO/${firebaseUser.uid}/logo`);
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/*'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                setUploadPercentage(progress.toFixed(2));
            }, () => {
                toast.error('Something went wrong', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                setCredentials({...credentials, companyLogo:url});
                await set(ref(firebaseDatabase, `COLLEGE_ARCHIVE/${firebaseUser.uid}/companyLogo`), url)
                toast.success('Profile picture updated', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                setUploadingImage(false);
            })

        }
        inputElement.click();

    }

    return(
        <div className='company-profile-container'>
            <div className='company-profile-content'>
                <h2 className='section-title' >Company Profile</h2>
                
                {
                    uploadingImage
                    ?
                    <div className='profile-logo' >
                        <Spinner animation="border" size={20} /> 
                        <span>{uploadPercentage}% Uploaded</span> 
                    </div>
                    :
                    <div className="image-container">
                        <Image src={credentials.companyLogo?credentials.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde"} onClick={handleLogoChange} className='profile-logo' roundedCircle />
                        <p className="picture-tag" onClick={handleLogoChange}>Click to change picture</p>
                    </div>
                }
                <Form className='main-form' onSubmit={handleSubmit} >
                    <Form.Group  className="mb-3 end-to-end">
                        <Form.Label className='text-label '>College Name</Form.Label>
                        <Form.Control required  size='lg' className='text-input' value={firebaseUser.displayName}  />
                    </Form.Group>

                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label'>Email Id</Form.Label>
                        <Form.Control required size='lg' disabled className='text-input' value={firebaseUser.email}  />
                    </Form.Group>
                    <Form.Group  className="mb-3 second-half" style={{display:"flex", flexDirection:'column'}}>
                        <Form.Label className='text-label'>Password</Form.Label>
                        <Form.Control name={'password'}  onChange={handleChange} disabled  size='lg' className='text-input' value="               " type='password' />
                        <Form.Text className="error-tag" onClick={()=>{setModalVisibility(true)}}>Reset password?</Form.Text>
                    </Form.Group>
                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label'>Placement Officer Name</Form.Label>
                        <Form.Control name='placementOfficerName' size='lg' onChange={handleChange}  className='text-input' value={credentials.placementOfficerName}  />
                    </Form.Group>
                    <Form.Group  className="mb-3 second-half">
                        <Form.Label className='text-label'>Placement Officer Phone Number</Form.Label>
                        <Form.Control name='placementOfficerPhoneNumber' type='tel' onChange={handleChange} size='lg' className='text-input' value={credentials.placementOfficerPhoneNumber}  />
                    </Form.Group>

                    <Form.Group className="mb-3 end-to-end " controlId="exampleForm.ControlTextarea1">
                        <Form.Label className='text-label'>College Address</Form.Label>
                        <Form.Control required name='collegeAddress' onChange={handleChange} value={credentials.collegeAddress} className='text-input' style={{minHeight:'200px'}} as="textarea" rows={3} />
                    </Form.Group>

                    <Button variant="primary" className='end-to-end' style={{width:'200px', margin:'0 auto'}} type="submit">
                        Save
                    </Button>

                    {/* <div className="end-to-end download-container" onClick={()=>{firebaseAuth.signOut()}}>
                        <FiLogOut size={25} color="#3761EE" />
                        <p className="download-tag">Logout</p>
                    </div> */}
                </Form>
            </div>
            <ToastContainer />
            <Modal show={modalVisibility} onHide={()=>{setModalVisibility(false)}}>
                <Modal.Header closeButton >
                  <Modal.Title>Send link?</Modal.Title>
                </Modal.Header>
                                
                <Modal.Body>
                  <p>Are you sure you want a password reset link sent to your email-Id ({credentials.emailId})?</p>
                </Modal.Body>
                                
                <Modal.Footer>
                  <Button variant="secondary" onClick={()=>{setModalVisibility(false)}}>Close</Button>
                  <Button variant="primary" onClick={handleForgot}>Send</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default CompanyProfileContainer;
import React, { useEffect, useState } from 'react';
import { firebaseAuth, firebaseDatabase } from '../../backend/firebase-handler';
import NumericalDetailsContainer from '../numerica-details-container/NumericalDetailsContainer.component';
import ProfileContainer from '../profile-container/ProfileContainer.component';
import TableContainer from '../table-container/TableContainer.component';
import './home-content-container.style.scss';
import { ref, get, child } from 'firebase/database';
import LatestJobsTableData from "../latest-jobs-table-data/LatestJobsTableData.component";
import { Table } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';


const HomeContentContainer = () => {
    const [loading, setLoading] = useState(true);
    const [tableItems, setTableItems] = useState([]);
    const [departmentList, setDepartmentList] = useState([])
    const [stats, setStats] = useState([])
    const firebaseUser = firebaseAuth.currentUser;
    const [selectedBranch, setSelectedBranch] = useState("")
    const navigate = useNavigate()


    useEffect(() => {
        (async () => {
 
            let tmepDepart = []
            var temp = []
            let count = 0
            get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+firebaseUser.uid)).then(async(snap)=>{
                if (snap.exists()) {
                    for (const key in snap.val()) {
                        tmepDepart.push(key)
                        count++
                        if (count === 1) {
                            let count2 = 0
                            for(const key2 in snap.child(key).val()) {
                                if (snap.child(key).child(key2).child("yearOfStudy").val() === "2022" && count2 <= 9) {
                                    temp.push(snap.child(key).child(key2).val())
                                    count2++
                                }
                            }
                        }
                    }
                    setDepartmentList(tmepDepart)
                    setStats(temp.reverse())
                    setLoading(false)
                    setSelectedBranch(tmepDepart[0])
                }
            })

            const jobListReference = ref(firebaseDatabase, `COLLEGE_VACANCY/${firebaseUser.uid}`);
            const snapshot = await get(jobListReference)
            if (snapshot.exists()){
                const data = [];
                for (const key in snapshot.val()) {
                    const job = snapshot.child(key).val();
                    data.push(job);
                }
                setTableItems(data.reverse());
                setLoading(false);
            }else {
                setLoading(false);
            }
        })()
    }, [])
    
    const handleBranch = async (item) => {
        console.log("aaa")
        var temp = []
        setSelectedBranch(item)
        await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+firebaseUser.uid+"/"+item)).then(async(snap)=>{
            if (snap.exists()) {
                let count2 = 0
                for(const key2 in snap.val()) {
                    if (snap.child(key2).child("yearOfStudy").val() === "2022" && count2 <= 9) {
                        temp.push(snap.child(key2).val())
                        count2++
                    }
                }
                setStats(temp.reverse())
            }
        })
    }
    return(
        <div className='home-content-container'>
            <div className='profile-container'>
                <ProfileContainer loading={loading} tableItems={tableItems} />
            </div>

            <div className='numeric-details-container'>
                <NumericalDetailsContainer />
            </div>

            <div className='table-container'>
                <p className='section-title'>Placement Stats</p>
                <div className="tabs-container">
                    {
                        departmentList.map((item, index)=>{return(
                            <p onClick={()=>{handleBranch(item)}} className={selectedBranch===item?"selected":"unselected"}>{item}</p>
                        )})
                    }
                </div>
                <Table style={{ marginTop: "10px" }} responsive hover >
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>USN</th>
                            <th>Semester</th>
                            <th>No. of offers</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            stats.map((item, index) => {return(
                                <tr onClick={()=>{navigate("/student-detail", {state:{item:item, type:"OFFERS"}})}}>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.picture?item.picture:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.name}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.usn}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.semester}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placed?item.placed.length:"0"}</div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>

            <div className='table-container'>
                <LatestJobsTableData loading={loading} tableItems={tableItems} />
            </div>
            
        </div>
    )
}

export default HomeContentContainer;
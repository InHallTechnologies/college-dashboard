import React, { useEffect, useState } from "react";
import { Button, Table, Spinner } from "react-bootstrap";

import "./LatestJobsTableData.style.scss";
import LatestJobsTableItems from "../latest-jobs-table-items/LatestJobsTableItems.component";


const LatestJobsTableData = ({ tableItems, loading }) => {
    const [screenSize, setScreenSize] = useState(window.innerWidth);
    useEffect(() => {


        window.addEventListener('resize', handleScreenSize);
        return () => {
            window.removeEventListener('resize', handleScreenSize);
        }

    }, [])

    const handleScreenSize = () => {
        setScreenSize(window.innerWidth);
    }



    return (
        <div className="table-content">
            <h2 className="section-title">
                Latest Jobs Posted
            </h2>

            {
                loading
                    ?
                    <div className='loading-container' >
                        <Spinner animation="border" size="sm" />
                    </div>
                    :
                    null
            }

            {
                !loading && tableItems.length !== 0
                    ?
                    <Table style={{ marginTop: "10px" }} responsive hover >
                        <thead>
                        <tr>
                            <th>Company</th>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>

                        </tr>
                        </thead>
                        <tbody>
                        {
                            tableItems.map((item) => <LatestJobsTableItems item={item} screenSize={screenSize} />)
                        }
                        </tbody>
                    </Table>
                    :
                    <div className="no-vacancy-container">
                        <span className="no-vacancy-label">No added vacancy</span>
                    </div>
            }
        </div>
    );
};

export default LatestJobsTableData;

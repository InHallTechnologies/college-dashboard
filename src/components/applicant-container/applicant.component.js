import { ref, set } from 'firebase/database'
import React from 'react'
import { Button, Form } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import { firebaseAuth, firebaseDatabase } from '../../backend/firebase-handler'
import './applicant.style.scss'

const Applicant = ({item, jobId, companyId, shortList, setShortList}) => {

    const navigate = useNavigate()
    const firebaseUser = firebaseAuth.currentUser

    const handleShortlist = async () => {
        if (item.status === "Processing") {
            await set(ref(firebaseDatabase, "COLLEGE_APPLICANTS_LIST/"+ firebaseUser.uid + "/" + companyId + "/" + jobId + "/" + item.key + "/status"), "Applied")
            await set(ref(firebaseDatabase, "APPLICATION_STATUS/" + item.key + "/" + jobId + "/status"), "Applied")  
        } else {
            await set(ref(firebaseDatabase, "COLLEGE_APPLICANTS_LIST/" + firebaseUser.uid + "/" + companyId + "/" + jobId + "/" + item.key + "/status"), "Processing")
            await set(ref(firebaseDatabase, "APPLICATION_STATUS/" + item.key + "/" + jobId + "/status"), "Processing")
        }
    }

    return(
        <tr>
            <td>
                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                    <img src={item.picture?item.picture:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde"} alt={item.name} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                    <p>{item.name}</p>
                </div>
            </td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.usn}</div></td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.collegeName}</div></td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.department}</div></td>
            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.status==="Placed"?"₹ "+item.package:"-"}</div></td>
            {
                item.status==="Applied" || item.status==="Processing"
                ?
                <td><div style={{display:"flex", flexDirection:"row", alignItems:"center", height:50}}>
                    <Button className="upgrade-button" variant="primary" onClick={()=>{navigate("/student-detail", {state:{item:item, jobId:jobId, companyId:companyId}})}}>Resume</Button>
                </div></td>
                :
                item.status==="Placed"
                ?
                <div style={{display:"flex", flexDirection:"row", alignItems:"center", height:70}}>
                    <Button className="upgrade-button" style={{backgroundColor:"#4BCD7F"}} variant="primary">Placed</Button>
                </div>
                :
                <div style={{display:"flex", flexDirection:"row", alignItems:"center", height:70}}>
                    <Button className="upgrade-button" style={{backgroundColor:"#FFB800"}} variant="primary">Rejected</Button>
                </div>
            }
            <td>
                {
                    (item.status ==="Placed" || item.status ==="Rejected")
                    ?
                    null
                    :
                    <Form.Check style={{display:"flex", alignItems:"center", height:50}} label="" name="group1" type={"checkbox"} id={"serviceAgreement"} checked={item.status === "Processing"} onChange={(event)=>{handleShortlist()}} />
                }
            </td>
        </tr>
    )
}

export default Applicant
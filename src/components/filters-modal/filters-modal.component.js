import React, { useState } from "react";
import { useEffect } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import './filters-modal.style.css';

function FiltersModal({ filters, setFilters }) {
    const [show, setShow] = useState(false);
    const [appliedFilters, setAppliedFilters] = useState({});

    useEffect(() => {
        setAppliedFilters({...filters, courseWise: filters.courses});
    }, [filters])

    useEffect(() => {
        if (show) {
            setFilters(state => ({...state, collegeName:[], branchName:[], graduationYear:[], courseWise:[...filters.courses] }))
        }
    }, [show])

    

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleApply = () => {
        setFilters(appliedFilters);
        handleClose();
    }

    const handleChange = (value, objectName) => {
        let newArray = appliedFilters[objectName];

        if (newArray.includes(value)){
            newArray = newArray.filter(item =>item !== value)
        }else {
            newArray.push(value);
        }

        setAppliedFilters(state => {
            return({
                ...state,
                [objectName]: newArray
            })
        })
    }

    const handleTextChange =(value, item, index) => {
        const temp = [...appliedFilters.courseWise]
        temp.splice(index, 1, {...appliedFilters.courseWise[index], score: parseFloat(value)});
        
        setAppliedFilters(state => {
            return ({
                ...state,
                courseWise:[...temp]
            })
        })
    }
    

    return (
        <div>
            <Button style={{marginRight:'10px'}} variant="primary" onClick={handleShow}>
                Add Filters
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                fullscreen={'xxl-down'}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Add Filters</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    You can select multiple filters

                    <div style={{marginTop:'20px',}}>
                        <h5 className="checkbox-filters-title">Filter by colleges</h5>
                        <div className="checkbox-filters-value">
                            {
                                filters.colleges.map(item => {
                                    return(
                                        <Form.Group key={item} onChange={(event) => handleChange(event.target.value, "collegeName")} className="mb-3 filter-checkbox" controlId="formBasicCheckbox">
                                            <Form.Check type="checkbox" value={item} checked={appliedFilters.collegeName.includes(item)?true:false} label={item} />
                                        </Form.Group>
                                    )
                                })
                            }
                        </div>
                    </div>

                    <div style={{marginTop:'20px',}}>
                        <h5 className="checkbox-filters-title">Filter by branch</h5>
                        <div className="checkbox-filters-value">
                            {
                                filters.branches.map(item => {
                                    return(
                                        <Form.Group onChange={(event) => handleChange(event.target.value, "branchName")} key={item} className="mb-3 filter-checkbox" controlId="formBasicCheckbox">
                                            <Form.Check type="checkbox" value={item} checked={appliedFilters.branchName.includes(item)?true:false} label={item} />
                                        </Form.Group>
                                    )
                                })
                            }
                        </div>
                    </div>

                    <div style={{marginTop:'20px',}}>
                        <h5 className="checkbox-filters-title">Filter by graduation year</h5>
                        <div className="checkbox-filters-value">
                            {
                                filters.graduationYears.map(item => {
                                    return(
                                        <Form.Group key={item} onChange={(event) => handleChange(event.target.value, "graduationYear")} className="mb-3 filter-checkbox" controlId="formBasicCheckbox">
                                            <Form.Check type="checkbox" checked={appliedFilters.graduationYear.includes(item)?true:false} value={item} label={item} />
                                        </Form.Group>
                                    )
                                })
                            }
                        </div>
                    </div>

                    <div style={{marginTop:'20px',}}>
                        <h5 className="checkbox-filters-title">Filter by Assessment Scores</h5>
                        <div className="checkbox-filters-value">
                            {
                                filters.courses.map((item, index) => {
                                    return(
                                        <div className="score-filter-holder">
                                            <p>{item.name}</p>
                                            <input placeholder="Enter score in %" type={'number'} onChange={event => {handleTextChange(event.target.value, item, index)}} />
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Dismiss
                    </Button>
                    <Button variant="primary" onClick={handleApply} >Apply Filters</Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
export default FiltersModal;
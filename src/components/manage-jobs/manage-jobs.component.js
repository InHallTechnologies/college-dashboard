import React, { useEffect, useState, useContext } from "react";
import './manage-jobs.style.scss'
import ProfileContainer from "../profile-container/ProfileContainer.component";
import SearchBar from "../search-bar/SearchBar.component";
import {Button, Spinner, Table} from "react-bootstrap";
import TableItems from "../table-items/TableItems.component";
import { get, ref, child } from 'firebase/database'
import { firebaseDatabase } from '../../backend/firebase-handler';
import Context from '../../context/appContext';

const ManageJobs = () => {

    const [loading, setLoading] = useState(true);
    const [jobList, setJobList] = useState([])
    const [originalList, setOriginalList] = useState([])
    const [screenSize, setScreenSize] = useState(window.innerWidth);
    const [globalState, dispatchForGlobalState] = useContext(Context)
    const [searchWord, setSearchWord] = useState("")

    useEffect(() => {
        window.addEventListener('resize', handleScreenSize);

        var temp = []
        get(child(ref(firebaseDatabase), "COLLEGE_VACANCY/" + globalState.uid)).then((snapShot)=>{
            if (snapShot.exists()) {
                for  (const key in snapShot.val()) {
                    temp.push(snapShot.child(key).val())
                }
                setJobList(temp.reverse())
                setOriginalList(temp)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
        return () => {
            window.removeEventListener('resize', handleScreenSize);
        }
    }, [])

    const onSearchEntered = () => {
        var temp = []
        for (const index in originalList) {
            var item = originalList[index]
            if (item.companyName.toLowerCase().includes(searchWord.toLowerCase())) {
                temp.push(item)
            }
            setJobList(temp)
        }
    }

    const handleScreenSize = () => {
        setScreenSize(window.innerWidth);
    }

    return(
        <div className={'placement-stats-container'} >
            <ProfileContainer />
            <div className={'content'} >
                <SearchBar placeholder={"Company Name"} onSearchEntered={onSearchEntered} searchWord={searchWord} setSearchWord={setSearchWord}  />
                <div className="table-content">
                    <h2 className="section-title">
                        Manage Jobs
                    </h2>

                    {
                        loading
                        ?
                        <div className='loading-container' >
                            <Spinner animation="border" size="sm" />
                        </div>
                        :
                        jobList.length === 0
                        ?
                        <div className="no-vacancy-container">
                            <span className="no-vacancy-label">No listings found</span>
                        </div>
                        :
                        <Table style={{ marginTop: "10px" }} responsive hover >
                            <thead>
                            <tr>
                                <th>Company</th>
                                <th>Role</th>
                                <th>Package</th>
                                <th>Location</th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                jobList.map((item) => <TableItems item={item} screenSize={screenSize} />)
                            }
                            </tbody>
                        </Table>
                    }
                </div>
            </div>
        </div>
    )
}

export default ManageJobs
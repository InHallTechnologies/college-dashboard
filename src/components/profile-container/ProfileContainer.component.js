import React, { useContext } from 'react';
import { Image, Dropdown } from 'react-bootstrap';
import { firebaseAuth } from '../../backend/firebase-handler';
import Context from '../../context/appContext';
import './ProfileContainer.style.scss';
import { useNavigate } from "react-router-dom";

const ProfileContainer = () => { 

    const firebaseUser = firebaseAuth.currentUser;
    const [globalState, dispatchForGlobalState] = useContext(Context);
    const navigate = useNavigate()

    return(
        <div className='profile-content' >
            <div className='name-container' >
                <h2 className='name'>{firebaseUser.displayName}</h2>
                <span className='designation'>{globalState.placementOfficerName?globalState.placementOfficerName:""}</span>
            </div>

            <Dropdown>
                <Dropdown.Toggle as='image' >
                    <Image id="dropdown-basic" roundedCircle className='profile-image' src={globalState.companyLogo?globalState.companyLogo:'https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde'} />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    <Dropdown.Item href="/?tab=ManageProfile">Profile</Dropdown.Item>
                    <Dropdown.Item onClick={()=>{navigate("/");firebaseAuth.signOut()}}>Logout</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            
        </div>
    )
}

export default ProfileContainer;
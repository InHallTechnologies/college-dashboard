import { child, get, ref } from 'firebase/database'
import React, { useContext, useEffect, useState } from 'react'
import { Button, Form, Spinner, Table } from 'react-bootstrap'
import { firebaseAuth, firebaseDatabase } from '../../backend/firebase-handler'
import Context from '../../context/appContext'
import ProfileContainer from '../profile-container/ProfileContainer.component'
import './report.style.scss'

const Report = () => {


    const [result, setResult] = useState([])
    const [loading, setLoading] = useState(true)
    const [selectedCompany, setSelectedCompany] = useState("")
    const [companyList, setCompanyList] = useState([])
    const [userData, setUserData] = useContext(Context)
    

    useEffect(()=>{
        let temp = []

        get(child(ref(firebaseDatabase), "COLLEGE_WISE_COMPANY/"+userData.uid)).then((snap)=>{
            if (snap.exists()) {
                for(const key in snap.val()) {
                    temp.push(snap.child(key).val())
                }
                setCompanyList(temp)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleFilter = () => {
        setLoading(true)
        let temp = []

        get(child(ref(firebaseDatabase), "COLLEGE_APPLICANTS_LIST/"+userData.uid+"/"+selectedCompany)).then((snap)=>{
            if (snap.exists()) {
                for (const key in snap.val()) {

                }
            }
        })
    }

    return(
        <div className='report-container'>
            <ProfileContainer/> 

            <div className={'content'} >
                {/* <SearchBar /> */}
                <div className="table-content">
                    <h2 className="section-title">
                        Placement Statistics
                    </h2>

                    <div className="filter-container">
                        <Form.Group className="mb-3 department-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Company</Form.Label>
                            <Form.Select value={selectedCompany} onChange={event => setSelectedCompany(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select company</option>
                                {
                                    companyList.map(item => <option key={item} value={item.uid}>{item.companyName}</option>)
                                }
                            </Form.Select>
                        </Form.Group>
                        <Button className="search-button" variant="primary" onClick={handleFilter}>Search</Button>
                    </div>

                    {
                        loading
                        ?
                        <div className='loading-container' >
                            <Spinner animation="border" size="sm" />
                        </div>
                        :
                        result.length === 0
                        ?
                        <div className="no-vacancy-container">
                            <span className="no-vacancy-label">No postings here!</span>
                        </div>
                        :
                        <Table style={{ marginTop: "10px" }} responsive="sm" hover >
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>No. of Postings</th>
                                    <th>Students Placed</th>
                                    <th>Average Package</th>
                                    <th>Highest Package</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    result.map((item, index) => {return(
                                        <tr>
                                            <td>
                                                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                    <img src={item.picture?item.picture:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                    <p>{item.name}</p>
                                                </div>
                                            </td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.usn}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.semester}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placed?item.placed.length:"0"}</div></td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }
                </div>
            </div>
        </div>
    )
}

export default Report
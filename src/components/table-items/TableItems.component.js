import React, { useState } from "react";
import { Button, Modal, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "./TableItems.style.scss";

const TableItems = ({ item, screenSize }) => {
    const [show, setShow] = useState(false);
    const navigate = useNavigate();
    
    const handleClose = () => {
        setShow(false);
    };
    const handleShow = () => setShow(true);

    const handleViewEdit = () => {
        navigate(`/job-list/${item.jobId}`)
        handleClose();
    }

    

    return (
        <>
            <tr className="table-item" style={{ cursor: "pointer" }} onClick={screenSize < 550?handleShow:null}>
                <td className="table-item-data">{item.companyName}</td>
                <td className="table-item-data">{item.role}</td>
                <td className="table-item-data">{item.startPackage}L - {item.endPackage}L</td>
                <td className="table-item-data">{item.location}</td>
                <td style={{display:show?"none":null}}><div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                    <Button className="upgrade-button" variant="primary" onClick={()=>{navigate("/job-list", {state:{type:"EDIT", job:item}})}}>View/Edit</Button>
                    <Button className="upgrade-button" style={{backgroundColor:"#52E1E2"}} variant="primary" onClick={()=>{navigate("/applications", {state:{id:item.jobId, company:item.companyUID, role:item.role, companyName:item.companyName}});}}>Application</Button>
                </div></td>
            </tr>
            <Modal centered onHide={handleClose} show={show}>
                <Modal.Header closeButton >
                    <Modal.Title>Job Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table  bordered hover variant="sm"  >
                        <thead>
                            <tr>
                                <th>Role</th>
                                <th>Package</th>
                                <th>Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{item.role}</td>
                                <td>{(parseFloat(item.startPackage)/100000).toFixed(1)}L - {(parseFloat(item.endPackage)/100000).toFixed(1)}L</td>
                                <td>{item.location}</td>
                            </tr>

                        </tbody>
                    </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleViewEdit}>
                        View/Edit
                    </Button>
                    <Button variant="primary" onClick={()=>{navigate("/applications", {state:{id:item.jobId, company:item.companyUID}});}}>
                        Applications
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default TableItems;

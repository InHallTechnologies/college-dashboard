import React, { useState } from "react";
import { Button, Modal, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "./LatestJobsTableItems.style.scss";

const LatestJobsTableItems = ({ item, screenSize }) => {
    const [show, setShow] = useState(false);
    const navigate = useNavigate();

    const handleClose = () => {
        setShow(false);
    };
    const handleShow = () => setShow(true);

    const handleViewEdit = () => {
        navigate("/job-list", {state:{type:"EDIT", job:item}})
        handleClose();
    }



    return (
        <>
            <tr className="table-item" style={{ cursor: "pointer" }} onClick={screenSize < 550?handleShow:null}>
                <td className="table-item-data">{item.companyName}</td>
                <td className="table-item-data">{item.role}</td>
                <td className="table-item-data">{item.startPackage}L - {item.endPackage}L</td>
                <td className="table-item-data">
                    {item.location}
                </td>
                {
                    screenSize >= 550
                        ?
                        <td className="table-action-items">
                            <div className="table-action-items-container">
                                <span className="table-actions primary" onClick={handleViewEdit}>View/Edit</span>
                                <span className="table-actions secondary" onClick={()=>{navigate("/applications", {state:{id:item.jobId, company:item.companyUID, role:item.role, companyName:item.companyName}});}}>Applications</span>
                            </div>
                        </td>
                        :
                        null
                }

            </tr>
            <Modal centered onHide={handleClose} show={show}>
                <Modal.Header closeButton >
                    <Modal.Title>Job Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table  bordered hover variant="sm"  >
                        <thead>
                        <tr>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{item.role}</td>
                            <td>{item.startPackage}L - {item.endPackage}L</td>
                            <td>{item.location}</td>
                        </tr>

                        </tbody>
                    </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleViewEdit}>
                        View/Edit
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Applications
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default LatestJobsTableItems;

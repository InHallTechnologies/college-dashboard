import React from 'react';
import AllJobsTableContainer from '../all-jobs-table.container/AllJobsTableContainer.component';
import ProfileContainer from '../profile-container/ProfileContainer.component';
import './AllJobsContainer.style.scss';

const AllJobsContainer = () => {

    return(
        <div className='all-post-content-container' >
            <ProfileContainer />
            <div>
                <AllJobsTableContainer />
            </div>
        </div>
    )
}

export default AllJobsContainer;
import React, {useEffect, useState} from 'react';
import './ManageCompanies.style.scss';
import ProfileContainer from "../profile-container/ProfileContainer.component";
import SearchBar from "../search-bar/SearchBar.component";
import {Button, Spinner, Table} from "react-bootstrap";
import { IoMdAdd } from 'react-icons/io'
import CompanyListArch from "../company-list-arch/CompanyListArch.component";
import {useNavigate} from "react-router-dom";
import {get, ref} from "firebase/database";
import {firebaseAuth, firebaseDatabase} from "../../backend/firebase-handler";

const ManageCompanies = ({type}) => {

    const [companyList, setCompanyList] = useState([]);
    const [originalList, setOriginalList] = useState([])
    const [title, setTitle] = useState(type==="VIEW"?"Manage Companies":"Select a company")
    const [loading, setLoading] = useState(true);
    const [searchWord, setSearchWord] = useState("")
    const [buttonTag, setButtonTag] = useState(type==="VIEW"?"Detail":"Select")
    const firebaseUser = firebaseAuth.currentUser;
    const navigate = useNavigate();

    useEffect(() => {
        const companyListReference = ref(firebaseDatabase,`COLLEGE_WISE_COMPANY/${firebaseUser.uid}`);
        get(companyListReference).then((snapshot) => {
            console.log("I am here")
            if (snapshot.exists()){
                const data = [];
                for (const companyId in snapshot.val()) {
                    const company = snapshot.child(companyId).val();
                    data.push(company);
                }
                setCompanyList(_ => data);
                setOriginalList(_ => data)
            }
            setLoading((false))
        })
    },[])

    const handleAddNewCompany = (companyId) => {
        navigate(`/company-details/${companyId}`);
    }

    const onSearchEntered = () => {
        var temp = []
        for (const index in originalList) {
            var item = originalList[index]
            if (item.companyName.toLowerCase().includes(searchWord.toLowerCase())) {
                temp.push(item)
            }
            setCompanyList(temp)
        }
    }

    return(
        <div className={'manage-company-container'}>
            <ProfileContainer />
            <div className={'manage-content'} >
                <SearchBar placeholder={"Company Name, HR Name"} onSearchEntered={onSearchEntered} searchWord={searchWord} setSearchWord={setSearchWord} />
                <div className={'company-list-container'}>
                    <div className="title-button-container">
                        <p className="section-title">{title}</p>
                        <Button variant="primary" className="button" style={type==="VIEW"?null:{display:"none"}} onClick={_ => handleAddNewCompany('add-company')} >Add company</Button>
                    </div>

                    {
                        loading
                        ?
                        <div className={'loading-container'}>
                            <Spinner animation="border" />
                        </div>
                        :
                        companyList.length === 0
                        ?
                        <div className={'loading-container'}>
                            <span className={'empty-label'}>No Companies added</span>
                        </div>
                        :
                        <Table hover className="table" responsive>
                            <thead>
                                <tr >
                                    <th>Company</th>
                                    <th>HR Name</th>
                                    <th>Phone Number</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody className="table-body">
                                {
                                    companyList.map((item, index) => {return(
                                        <tr>
                                            <td>
                                                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                    <img src={item.companyLogo?item.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fbuildings.png?alt=media&token=41b80aa2-3348-4616-8b04-5f155d9e0b55"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                    <p>{item.companyName}</p>
                                                </div>
                                            </td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.hrName}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.hrContactNumber}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{if(type==="VIEW"){navigate(`/company-details/${item.uid}`)} else{navigate("/job-list", {state:{company:item, type:"ADD"}})}}}>{buttonTag}</Button></div></td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }
                </div>
            </div>
        </div>
    )
}

export  default  ManageCompanies;
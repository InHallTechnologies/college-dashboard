import React, {useContext, useEffect, useState} from "react";
import './Signup-box.style.scss';
import { Button, Spinner } from "react-bootstrap";
import validator from 'validator';
import { Link, useNavigate } from "react-router-dom";
import { createUserWithEmailAndPassword, updateProfile } from 'firebase/auth'
import { firebaseAuth, firebaseDatabase } from "../../backend/firebase-handler";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { ref, set } from "firebase/database";
import Context from "../../context/appContext";

const SignupBox = () => {

    const navigate = useNavigate();
    const [confirmPassword, setConfirmPassword] = useState("")
    const [credentials, setCredentials] = useState({
        collegeName: "",
        uid:'',
        emailId:'',
        collegeAddress:'',
        placementOfficerName:'',
        placementOfficerPhoneNumber:'',
        password:''
    })

    const [errorSet, setErrorSet] = useState({
        emailError:'',
        passwordError:"",
        confirmError:"",
        collegeAddressError:'',
        placementOfficerPhoneNumberError:'',
        collegeNameError:'',
    })
    const [globalState, dispatchForGlobalState] = useContext(Context);

    const [buttonLoading, setButtonLoading] = useState(false);

    useEffect(() => {
        firebaseAuth.onAuthStateChanged(user => {
            if (user) {
                navigate('/')
            }
        })
    }, [])

    const handleSubmit = (event) => {
        if (!validator.isEmail(credentials.emailId)){
            setErrorSet(errors => ({...errors, emailError:"Please enter a valid email id"}));
            return;
        }

        if (credentials.password.length < 8) {
            setErrorSet(errors => ({...errors, passwordError:"Minimum password width should be 8"}));
            return;
        }
        if (credentials.password !== confirmPassword) {
            setErrorSet(errors => ({...errors, confirmError:"Passwords do not match"}));
            return;
        }
        if (!credentials.collegeName){
            setErrorSet(errors => ({...errors, collegeNameError:"Please provide college name"}));
            return;
        }

        if (!validator.isMobilePhone(credentials.placementOfficerPhoneNumber)){
            setErrorSet(errors => ({...errors, placementOfficerPhoneNumberError:"Please provide hr contact number"}));
            return;
        }
        
        if (!credentials.collegeAddress){
            setErrorSet(errors => ({...errors, collegeAddressError:"Please provide college address"}));
            return;
        }

        createAccount();
    }

    const createAccount = async () => {
        try{
            setButtonLoading(true);
            const result =  await createUserWithEmailAndPassword(firebaseAuth, credentials.emailId, credentials.password);
            toast.success(`Welcome ${credentials.collegeName}. Your account is successfully created`, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            
            await updateProfile(result.user, { displayName: credentials.collegeName });
            const companyArchiveRef = ref(firebaseDatabase,`COLLEGE_ARCHIVE/${result.user.uid}`);
            credentials.password = null
            credentials.uid = result.user.uid
            credentials.companyLogo = "https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COLLEGE_LOGO%2Fcollege.png?alt=media&token=4b433e19-8007-4c8c-ad03-c6bd70d1f113"
            dispatchForGlobalState({type:'SET_GLOBAL_USER_DATA', payload: {...credentials}});
            await set(companyArchiveRef, { ...credentials })
        }catch(err) {
            if (err.message.includes('auth/email-already-in-use')){
                toast.error("Account with this email id already exists.", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }else {
                toast.error('Something went wrong. Please try later', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        }
        setButtonLoading(false);

    }

    const handleChange = (event) => {
        const { value, name } = event.target;
        setErrorSet({ emailError:'',
            passwordError:"",
            collegeAddressError:'',
            placementOfficerPhoneNumberError:'',
            collegeNameError:'',
            confirmError:''})
        setCredentials({
            ...credentials,
            [name]:value
        })
    }

    return(
        <div className="signup-box-container" >
            
            <div className="input-container">
                <label className="label" >Email Id</label>
                <input onChange={handleChange} name="emailId"  className="input" id='emailId' placeholder="someone@example.com" type={'email'} />
                {
                    errorSet.emailError
                    ?
                    <span className="error-label" >{errorSet.emailError}</span>
                    :
                    null
                }
            </div>

            <div className="input-container">
                <label className="label" >Password</label>
                <input onChange={handleChange} name="password"  className="input password" id='password' placeholder="**********" type={'password'} />       
                {
                    errorSet.passwordError
                    ?
                    <span className="error-label" >{errorSet.passwordError}</span>
                    :
                    null
                }
            </div>

            <div className="input-container">
                <label className="label" >Confirm Password</label>
                <input onChange={(event)=>{setConfirmPassword(event.target.value); setErrorSet(state => ({...state, confirmError:''})) }} name="password" className="input password" id='password' placeholder="**********" type={'password'} />       
                {
                    errorSet.confirmError
                    ?
                    <span className="error-label" >{errorSet.confirmError}</span>
                    :
                    null
                }
            </div>

            <div className="input-container">
                <label className="label" >College Name</label>
                <input onChange={handleChange} name="collegeName"  className="input" id='company' placeholder="College full name"  />
                {
                    errorSet.collegeNameError
                    ?
                    <span className="error-label" >{errorSet.collegeNameError}</span>
                    :
                    null
                }
            </div>

            <div className="input-container">
                <label className="label" >Placement Officer Number</label>
                <input onChange={handleChange} maxLength={10} name="placementOfficerPhoneNumber"  className="input" id='phoneNumber' placeholder="+91 XXXXXXXXXX" type={'tel'} />
                {
                    errorSet.placementOfficerPhoneNumberError
                    ?
                    <span className="error-label" >{errorSet.placementOfficerPhoneNumberError}</span>
                    :
                    null
                }
            </div>

            

           

            <div className="input-container">
                <label className="label">College address</label>
                <textarea name="collegeAddress" onChange={handleChange} required className="input address-field " id='address' placeholder="" />
                {
                    errorSet.collegeAddressError
                    ?
                    <span className="error-label" >{errorSet.collegeAddressError}</span>
                    :
                    null
                }
            </div>
            
            

            <div className="button-container">
                <Button disabled={buttonLoading} onClick={handleSubmit} className="login-button" type="submit" variant="primary">
                    {
                        buttonLoading
                        ?
                        <Spinner  animation="border" size="sm" color="white" />
                        :
                        <span>Create Account</span>
                    }
                    
                </Button>
                
                <span className="continue-login-span">Already have an account? <Link to={'/login'} >Login</Link></span>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar = {false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />        
        </div>
    )
}

export default SignupBox;
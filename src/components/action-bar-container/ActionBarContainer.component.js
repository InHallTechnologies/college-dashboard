import React, { useContext, useEffect } from 'react';
import './ActionBarComponent.style.scss';
import logo from '../../assets/logo.png';
import { Image, Dropdown } from 'react-bootstrap';
import { AiOutlineArrowLeft } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import Context from '../../context/appContext';

const ActionBarComponent = () => {

    const navigate = useNavigate();
    const [globalState, dispatchForGlobalState] = useContext(Context);

    useEffect(() => {
        if (!globalState){
            navigate('/login')
        }
    }, [])


    const handleBack = () => {
        navigate(-1);
    }

    const goToHome = () => {
        navigate('/');
    }

    return(
        <div className='action-bar-component'>
           
            <div>
                <AiOutlineArrowLeft style={{cursor:'pointer'}} size={20} onClick={handleBack} />
                <Image onClick={goToHome} className='company-logo' src={logo} />
            </div>
            <Dropdown>
                    <Dropdown.Toggle as='image' >
                        <Image id="dropdown-basic" roundedCircle className='profile-image' src={globalState.companyLogo?globalState.companyLogo:'https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde'} />
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item href="/?tab=CompanyProfile">Profile</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">Logout</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
        </div>
    )
}

export default ActionBarComponent;
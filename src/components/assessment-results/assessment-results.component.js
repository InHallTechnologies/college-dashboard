import React, { useEffect, useState } from "react";
import ProfileContainer from "../profile-container/ProfileContainer.component";
import './assessment-results.style.scss'
import exportFromJSON from "export-from-json";
import { child, get, ref } from "firebase/database";
import { firebaseAuth, firebaseDatabase } from "../../backend/firebase-handler";
import { Form, Spinner, Table, Dropdown } from "react-bootstrap";

const AssessmentResults = () => {

    const [courses, setCourses] = useState([])
    const [loading, setLoading] = useState(true)
    const [selected, setSelected] = useState("")
    const [resultList, setResultList] = useState([])
    const [originalList, setOriginalList] = useState([])
    const [departments, setDepartments] = useState(["All branches"])
    const [selectDept, setSelectedDept] = useState("All branches")

    useEffect(() => {
        var tempAssmnt = []

        get(child(ref(firebaseDatabase), "ASSESSMENT_DETAILS")).then( async (snap) => {
            if (snap.exists()) {
                for (const key in snap.val()) {
                    tempAssmnt.push(snap.child(key).val())
                }
                await get(child(ref(firebaseDatabase), "PREQUALIFYING_EXAMS")).then((snap) => {
                    if (snap.exists()) {
                        for (const key in snap.val()) {
                            tempAssmnt.push(snap.child(key).val())
                        }
                        setLoading(false)
                        setCourses(tempAssmnt)
                    } else {
                        setLoading(false)
                    }
                })
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleSelect = async (value) => {
        var tempResult = []
        let tempDepartments = ["All branches"]
        setLoading(true)
        await get(child(ref(firebaseDatabase), "ASSESSMENT_RESULTS")).then((snap) => {
            if (snap.exists()) {
                for (const id in snap.val()) {
                    if (snap.child(id).child(value).exists() && snap.child(id).child(value).child("student").child("collegeUid").val() === firebaseAuth.currentUser.uid) {
                        tempResult.push(snap.child(id).child(value).val())
                        if (!tempDepartments.includes(snap.child(id).child(value).child("student").child("department").val())) {
                            tempDepartments.push(snap.child(id).child(value).child("student").child("department").val())
                        }
                    }
                }
                tempResult.sort((a,b) => (a.timeStamp > b.timeStamp) ? 1 : ((b.timeStamp > a.timeStamp) ? -1 : 0))
                setDepartments(tempDepartments)
                setResultList(tempResult.reverse())
                setOriginalList(tempResult)

                setSelectedDept("All branches")
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }
    console.log(resultList, originalList)

    const exportAll = (range) => {
        if (resultList.length === 0) {
            alert("No data to exports")
        } else {
            const jsonArray = []
            const fileName = 'Assessment Scores-' + resultList[0].courseName     
            const exportType = 'csv'  
            for (const index in resultList) {
                if (range === "all") {
                    jsonArray.push({"Student Name":removeComma(resultList[index].student.name.toString()), USN:removeComma(resultList[index].student.usn.toString()), College:removeComma(resultList[index].student.collegeName.toString()), Department:removeComma(resultList[index].student.department.toString()), Semester:removeComma(resultList[index].student.semester.toString()), "Year of Graduation":removeComma(resultList[index].student.yearOfStudy.toString()), Percentage:removeComma(resultList[index].percentage.toString()), "Phone Number":removeComma(resultList[index].student.phoneNumber.toString()), "Email-ID":removeComma(resultList[index].student.emailId.toString())});
                } else {
                    if (resultList[index].badge === range) {
                        jsonArray.push({"Student Name":removeComma(resultList[index].student.name.toString()), USN:removeComma(resultList[index].student.usn.toString()), College:removeComma(resultList[index].student.collegeName.toString()), Department:removeComma(resultList[index].student.department.toString()), Semester:removeComma(resultList[index].student.semester.toString()), "Year of Graduation":removeComma(resultList[index].student.yearOfStudy.toString()), Percentage:removeComma(resultList[index].percentage.toString()), "Phone Number":removeComma(resultList[index].student.phoneNumber.toString()), "Email-ID":removeComma(resultList[index].student.emailId.toString())});
                    }
                }
            }
            exportFromJSON({ data:jsonArray, fileName, exportType })  
        }
    }

    const removeComma = (input) => {
        return input.split(",").join(" ");
    }

    const handleBranch = (value) => {
        let tempList = []
        if(value !== "All branches") {
            for (const index in originalList) {
                if(originalList[index].student.department === value) {
                    tempList.push(originalList[index])
                }
            } 
            setResultList(tempList)
        } else {
            setResultList(originalList)
        }
    }

    return(
        <div className="assessment-results-container">
            <ProfileContainer />

            <div className='content' >
                <div className="picker-dropdown-container">
                    <div className="filters-container">
                        <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Select Course</Form.Label>
                            <Form.Select value={selected} onChange={event => {setSelected(event.target.value); handleSelect(event.target.value)}} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select course</option>
                                {
                                    courses.map(item => <option key={item.id} value={item.id} >{item.courseName}</option>)
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Select Department</Form.Label>
                            <Form.Select value={selectDept} onChange={event => {setSelectedDept(event.target.value); handleBranch(event.target.value)}} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select department</option>
                                {
                                    departments.map(item => <option key={item} value={item} >{item}</option>)
                                }
                            </Form.Select>
                        </Form.Group>
                    </div>

                    <Dropdown className="export-button">
                        <Dropdown.Toggle variant="primary" id="dropdown-basic">
                            Export score wise
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item onClick={()=>{exportAll("all")}}>All data</Dropdown.Item>
                            <Dropdown.Item onClick={()=>{exportAll("green")}}>90% - 100%</Dropdown.Item>
                            <Dropdown.Item onClick={()=>{exportAll("orange")}}>50% - 90%</Dropdown.Item>
                            <Dropdown.Item onClick={()=>{exportAll("red")}}>0% - 50%</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
                {
                    loading
                    ?
                    <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center"}} />
                    :
                    resultList.length === 0
                    ?
                    <p style={{marginTop:50, marginBottom:20, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC"}}>No one has taken this assessment yet!</p>
                    :
                    <Table hover className="table" responsive>
                        <thead>
                            <tr >
                                    <th>Name</th>
                                    <th>USN</th>
                                    <th>Department</th>
                                    <th>College</th>
                                    <th>Percentage</th>
                            </tr>
                        </thead>
                        <tbody className="table-body">
                            {
                                resultList.map((item, index) => {return(
                                    <tr>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.name}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.usn}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.department}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.collegeName}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50, fontWeight:700, color:item.badge==="red"?"#FF8585":item.badge==="green"?"#82ECAC":"#FEE4A3"}}>{item.percentage}%</div></td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </Table>
                }
            </div>
        </div>
    )
}

export default AssessmentResults
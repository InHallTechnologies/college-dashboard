import './SearchBar.style.scss';
import {BiSearch} from "react-icons/bi";
import React, {useState} from "react";

const SearchBar = ({ placeholder, onSearchEntered, searchWord, setSearchWord }) => {

    const handleChange = event => {
        setSearchWord(event.target.value);
    }



    return(
        <div className='search-bar-container'>
            <BiSearch size={24} color={"#444"} />
            <input placeholder={placeholder} onKeyUp={(event)=>{if(event.keyCode===13){onSearchEntered()}}} className={'search-input'} value={searchWord} onChange={handleChange}  />
        </div>
    )
}

export default SearchBar;
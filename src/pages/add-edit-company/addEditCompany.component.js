import React, { useState, useEffect, useContext } from 'react';
import './addEditCompany.style.scss';
import { Form, Button, Image, Spinner } from 'react-bootstrap';
import { firebaseAuth, firebaseDatabase, firebaseStorage } from '../../backend/firebase-handler';
import { get, ref, set, push } from 'firebase/database';
import { ToastContainer, toast } from "react-toastify";
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import {useNavigate, useParams} from "react-router-dom";
import logo from '../../assets/logo.png'
import { IoMdArrowRoundBack } from 'react-icons/io'


const CompanyProfileContainer = () => {

    const firebaseUser = firebaseAuth.currentUser;
    const [credentials, setCredentials] = useState({
        companyName:'',
        email:'',
        password:'',
        companyAddress:'',
        hrContactNumber:'',
        hrName:"",
        companyLogo:'https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fcompany.png?alt=media&token=5e1ff6bf-3e02-46b1-b254-192387511f24',
        uid:''
    });
    const [uploadingImage, setUploadingImage] = useState(false);
    const params = useParams();
    const navigate = useNavigate();
    const [uploadPercentage, setUploadPercentage] = useState(0);
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        const { companyId } = params;
        if (companyId === 'add-company') {
            (async () => {
                const collegeCompanyRef = ref(firebaseDatabase, `COLLEGE_WISE_COMPANY/${firebaseUser.uid}`);
                const key = await push(collegeCompanyRef).key
                setCredentials(state =>  ({...state, uid: key}));
            })()
        }else {
            (async () => {
                const collegeCompanyRef = ref(firebaseDatabase, `COLLEGE_WISE_COMPANY/${firebaseUser.uid}/${companyId}`);
                get(collegeCompanyRef).then(snapshot => {
                    if (snapshot.exists()){
                        const company = snapshot.val();
                        setCredentials({...company});
                    }
                })
            })()
        }
    }, [])

    const handleChange = (event) => {
        const { name, value } = event.target;
        setCredentials({...credentials, [name]: value });
    }

    const handleSubmit = async (event) => {
        setLoading(true)
        event.preventDefault();
        const profileRef = ref(firebaseDatabase, `COLLEGE_WISE_COMPANY/${firebaseUser.uid}/${credentials.uid}`);
        await set(profileRef, {...credentials});
        toast.success(`Saved Changes`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

    }

    const handleLogoChange = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            const logoRef = firebaseStorageRef(firebaseStorage, `COMPANY_LOGO/${firebaseUser.uid}/${credentials.uid}`);
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/*'});
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                setUploadPercentage(progress.toFixed(2));
            }, () => {
                toast.error('Something went wrong', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                setCredentials({...credentials, companyLogo:url});
                setUploadingImage(false);
            })

        }
        inputElement.click();

    }

    return(
        <div className='company-profile-container' style={{marginTop:0}}>

            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>   

            <div className='company-profile-content'>
                {
                    uploadingImage
                        ?
                        <div className='profile-logo' >
                            <Spinner animation="border" size={20} />
                            <span>{uploadPercentage}% Uploaded</span>
                        </div>
                        :
                        <div className='profile-logo-container'>
                            <Image src={credentials.companyLogo} className={'profile-logo'} onClick={handleLogoChange}  roundedCircle />
                            <span className={'click-upload-message'}>Click to upload company logo</span>
                        </div>
                }
                <Form className='main-form' onSubmit={handleSubmit} >
                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label '>Company Name</Form.Label>
                        <Form.Control required name={'companyName'} onChange={handleChange}  size='lg' className='text-input' value={credentials.companyName}  />
                    </Form.Group>

                    <Form.Group  className="mb-3 second-half">
                        <Form.Label className='text-label'>Email Id</Form.Label>
                        <Form.Control required name={'email'} onChange={handleChange}   size='lg' className='text-input' value={credentials.email}  />
                    </Form.Group>
                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label'>H.R. Name</Form.Label>
                        <Form.Control name='hrName'  size='lg' onChange={handleChange}  className='text-input' value={credentials.hrName}  />
                    </Form.Group>
                    <Form.Group  className="mb-3 second-half">
                        <Form.Label className='text-label'>H.R. Phone Number</Form.Label>
                        <Form.Control name='hrContactNumber' maxLength={10} type='tel' onChange={handleChange} size='lg' className='text-input' value={credentials.hrContactNumber}  />
                    </Form.Group>

                    <Form.Group className="mb-3 end-to-end " controlId="exampleForm.ControlTextarea1">
                        <Form.Label className='text-label'>Company Address</Form.Label>
                        <Form.Control required name='companyAddress' onChange={handleChange} value={credentials.companyAddress} className='text-input' style={{minHeight:'200px'}} as="textarea" rows={3} />
                    </Form.Group>

                    <Button variant="primary" disabled={loading} className='end-to-end' style={{width:'200px', margin:'0 auto'}} type="submit">
                        Save
                    </Button>
                </Form>
            </div>
            <ToastContainer />
        </div>
    )
}

export default CompanyProfileContainer;
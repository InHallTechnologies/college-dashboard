import React, {useContext, useState} from 'react';
import './upload-student-spreadsheet.style.scss';
import ActionBarComponent from "../../components/action-bar-container/ActionBarContainer.component";
import uploading from '../../assets/uploading.png';
import { readSpreadSheet } from "../../task-handlers/spreadsheet-reader";
import {Button, Spinner, Form, Table } from "react-bootstrap";
import StudentsListArch from "../../components/students-list-arch/StudentsListArch.component";
import {firebaseAuth, firebaseDatabase} from "../../backend/firebase-handler";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { push, ref, set} from "firebase/database";
import {useLocation, useNavigate} from "react-router-dom";
import branchList from '../../entities/branch-list';
import logo from "../../assets/logo.png"
import DatePicker from 'react-datepicker';
import Context from '../../context/appContext';
import { IoMdArrowRoundBack } from 'react-icons/io'


const UploadStudentSpreadsheet = () => {
    const [studentList, setStudentList] = useState([]);
    const firebaseUser = firebaseAuth.currentUser;
    const [uploadingStudentData, setUploadingStudentData] = useState(false);
    const [branch, setBranch] = useState(branchList[0])
    const [year, setYear] = useState(new Date())
    const [globalState, dispatchForGlobalState] = useContext(Context);
    const navigate = useNavigate()

    console.log(branch, year)

    const handleClick = () => {
        const element = document.createElement('input');
        element.setAttribute('type', "file");
        element.setAttribute('accept', '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel');
        element.onchange = async (event) => {
            const files =  event.target.files;
            readSpreadSheet(files[0], response => {
                if (response === 'error'){
                    toast.error('Invalid Spreadsheet', {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    return
                }
                const newResponse = response.map(item => {
                    const studentRef = ref(firebaseDatabase, `COLLEGE_WISE_STUDENTS/${firebaseUser.uid}/${branch}`);
                    const key = push(studentRef).key;
                    let date = new Date().getDate(); 
                    let month = new Date().getMonth() + 1; 
                    let year2 = new Date().getFullYear(); 
                    let hours = new Date().getHours(); 
                    let min = new Date().getMinutes();
                    let sec = new Date().getSeconds(); 
                    let finalDate = date + "-" + month + "-" + year2 + " " + hours + ":" + min + ":" + sec
                    return {...item, department:branch, semester:item.semester.toString(), phoneNumber:item.phoneNumber.toString(), date:finalDate, collegeKey:key, collegeName:globalState.collegeName, collegeUid:globalState.uid, uid:key, yearOfStudy:year.getFullYear().toString(), plan:"Free Plan", accountType:"DIRECT", key:key}
                })
                setStudentList(newResponse)
            });
        }
        element.click();
    } 

    const handleReset = () => {
        setStudentList([])
    }

    const handleConfirm = async () => {
        setUploadingStudentData(true);
        try{
            const tasks = [];
            for (const studentListElement of studentList) {
                const studentRef = ref(firebaseDatabase, `COLLEGE_WISE_STUDENTS/${firebaseUser.uid}/${branch}/${studentListElement.uid}`);
                tasks.push(set(studentRef, studentListElement))
            }
            Promise.all(tasks).then(() => {
                toast.success('All students data uploaded', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                handleReset()
                setUploadingStudentData(false);
            })
            var tempKey = push(ref(firebaseDatabase, "LOG/COLLEGE/")).key
            let date = new Date().getDate(); 
            let month = new Date().getMonth() + 1; 
            let year = new Date().getFullYear(); 
            let hours = new Date().getHours(); 
            let min = new Date().getMinutes();
            let sec = new Date().getSeconds(); 
            let logDate = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
            let tempObj = {key:tempKey, date:logDate, id:globalState.uid, name:globalState.collegeName, url:globalState.companyLogo, type:"STUDENTS ADDED"}
            await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/log/"+tempKey), tempObj)
            await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/name"), tempObj.name)

        }catch (err){

        }
    }

    return (
        <div className='upload-spreadsheet-container'>
            
            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div> 

            <div className={'upload-spreadsheet-content'}>
                <p className='title'>{studentList.length === 0? 'Upload Student data':'Please confirm and upload'}</p>
                <div className={'action-container'}>
                    {
                        studentList.length === 0
                        ?
                        <p className='sub-title'>Please upload student excel data. You can download the sample excel sheet <u style={{cursor:'pointer '}} onClick={()=>{window.open("https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/OccuHire%20Student%20Detail%20Sample%20Spreadsheet.xlsx?alt=media&token=a61b06c3-0ff3-4ee2-a313-7f53f6fd3afb", "_blank")}} >from here</u></p>
                        :
                        <p className='sub-title'>Verify the student records and upload data</p>

                    }
                </div>

                {
                    studentList.length === 0
                    &&
                    <div className="filter-container">
                        <Form.Group className="mb-3 year-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Graduation Year</Form.Label>
                            <DatePicker
                                selected={year}
                                onChange={(date) => setYear(date)}
                                showYearPicker
                                dateFormat="yyyy"
                                className='picker-input'
                            />
                        </Form.Group>
                        <Form.Group className="mb-3 department-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Department</Form.Label>
                            <Form.Select value={branch} onChange={event => setBranch(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select department</option>
                                {
                                    branchList.map(item => <option key={item} >{item}</option>)
                                }
                            </Form.Select>
                        </Form.Group>
                    </div> 
                }

                {
                    studentList.length !== 0
                    ?
                    <div className={'student-list-container'}>
                        <div className={'student-list-title-container'}>
                            <div className={'college-name-container'}>
                                <h4 style={{margin:0}}>Student List</h4>
                                <span className={'college-name'}>{firebaseUser.displayName}</span>
                            </div>
                            <div className={'button-container'}>
                                <Button disabled={uploadingStudentData} style={{width:'190px'}} className={'confirm-button'} onClick={handleConfirm} >
                                    {
                                        uploadingStudentData
                                        ?
                                        <Spinner animation={'border'} size={'sm'} />
                                        :
                                        <span>Confirm and Upload</span>
                                    }
                                </Button>
                                {
                                    !uploadingStudentData
                                    ?
                                    <Button onClick={handleReset} variant="outline-danger">Reset</Button>
                                    :
                                    null
                                }
                            </div>
                        </div>
                        <div className={'student-lists-directory'}>
                            <Table hover className="table" responsive >
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>USN</th>
                                    <th>Email-Id</th>
                                    <th>Phone Number</th>
                                    <th>Semester</th>
                                </tr>
                                </thead>
                                <tbody className="table-body">
                                {
                                    studentList.map(item => {return(
                                        <tr>
                                            <td>
                                                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                    <img src={"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fstudent.png?alt=media&token=59f398c7-546c-445d-8525-0294a6e0d053"} alt={item.name} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                    <p>{item.name}</p>
                                                </div>
                                            </td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.usn}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.emailId}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.phoneNumber}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.semester}</div></td>
                                        </tr>
                                    )})
                                }
                                </tbody>
                            </Table>
                        </div>
                    </div>
                    :
                    <div className={'uploader-box'} onClick={handleClick} >
                        <img className={'uploader-icon'} src={uploading} alt={'upload student spreadsheet'}  />
                        <span className={'uploader-label'}>Click to upload the spreadsheet</span>
                    </div>
                }

                <div className={'bottom-button-container'}>
                    <Button disabled={uploadingStudentData} style={{width:'190px', marginRight:'20px'}} className={'confirm-button'} onClick={handleConfirm} >
                        {
                            uploadingStudentData
                                ?
                                <Spinner animation={'border'} size={'sm'} />
                                :
                                <span>Confirm and Upload</span>
                        }
                    </Button>
                    {
                        !uploadingStudentData
                            ?
                            <Button onClick={handleReset} variant="outline-danger">Reset</Button>
                            :
                            null
                    }
                </div>
            </div>


            <ToastContainer />
        </div>
    )
}

export  default  UploadStudentSpreadsheet;
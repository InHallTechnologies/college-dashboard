import React, { useEffect, useState } from 'react';
import logo from '../../assets/logo.png'
import LoginBox from '../../components/login-box/LoginBox.component';
import './login.style.scss';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { get, ref } from 'firebase/database';
import { firebaseDatabase } from '../../backend/firebase-handler';
import heroImage from '../../assets/login-hero-image.png';
import feature from '../../assets/connect_with.png';
import { Image } from 'react-bootstrap';
import SimpleGridWithTitle from '../../components/simple-grid-with-title/SimpleGridWithTitle.component';
import FooterContainer from '../../components/footer/Footer.component';


const LoginPage = ({ history }) => {
    const [logoList, setLogoList] = useState({recruiters:[], universities:[]});

    useEffect(() => {
        const recruitersCommit = new Promise( async (resolve, reject) => {
            fetchLogos('LOGO_ARCHIVE/RECRUITER',resolve)
        })

    
        Promise.all([recruitersCommit]).then(data => {
            setLogoList({recruiters: data[1], universities:data[0]})
        })
       
    }, [])

    const fetchLogos = async (url, resolve) => {
        const logoRef = ref(firebaseDatabase, url);
        const logoSnapshot = await get(logoRef);
        if (logoSnapshot.exists()) {
            const list = await logoSnapshot.val();
            resolve(list);
        }else {
            resolve([]);
        }
    }

    return(
        <div className='login-container'>
            <Helmet>
                <title>Login | OccuHire</title>
            </Helmet>
            <header className='header-container' >
                <img className='logo' src={logo} alt='OccuHire'  />
            </header>
            <main className='main-container' >
                <div className='hero-illustration-container'>
                    <img className='hero-image' src={heroImage} />
                </div>
                <div className='main-box' >
                <p className='welcome-tag' >Welcome to OccuHire University</p>
                    <p className='sub-tag'>Login to continue</p>
                    <LoginBox />
                    <hr className="separator" />
                    <div className='bottom-options' >
                        <span className='labels' >Don't have an account?</span>
                        <span className='labels label-button' ><Link to={'/signup'} className='new-account-label' >Create one here</Link></span>
                    </div>
                </div> 
            </main>

            <div className="login-feature-container">
                <Image className='feature-image' src={feature} />
            </div>

            <SimpleGridWithTitle title="Our Students Work at" list={[...logoList.universities]} />
            <FooterContainer />
        </div>
    )
}

export default LoginPage;
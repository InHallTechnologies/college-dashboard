import React, { useContext, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./applications.style.scss"
import { child, get, onValue, push, query, ref, set } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import logo from '../../assets/logo.png'
import { Button, Dropdown, Spinner, Table } from "react-bootstrap";
import Context from "../../context/appContext";
import Applicant from "../../components/applicant-container/applicant.component";
import exportFromJSON from "export-from-json";
import FiltersModal from "../../components/filters-modal/filters-modal.component";
import { IoMdArrowRoundBack } from 'react-icons/io'

const Applications = () => {

    const location = useLocation()
    const [applications, setApplications] = useState([])
    const jobId = location.state.id
    const companyId = location.state.company
    const [loading, setLoading] = useState(true)
    const [globalState, dispatchForGlobalState] = useContext(Context)
    const [shortList, setShortList] = useState([])
    const role = location.state.role
    const companyName = location.state.companyName
    const [filters, setFilters] = useState({collegeName:[], branchName:[], graduationYear:[], courseWise:[], courses:[], colleges:[], branches:[], graduationYears:[]});
    const [allApplicants, setAllApplicants] = useState([]);
    const navigate = useNavigate()

    useEffect(()=>{
        var temp = []
        var tempShortlist = []
        const queryST = query(ref(firebaseDatabase, "COLLEGE_APPLICANTS_LIST/"+globalState.uid+"/"+companyId+"/"+jobId))
        onValue(queryST, (snapShot)=>{
            temp = []
            tempShortlist = []
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    temp.push(snapShot.child(key).val())
                    if (snapShot.child(key).child("status").val() === "Processing") {
                        tempShortlist.push(snapShot.child(key).val())
                    }
                }
                setApplications(temp)
                setAllApplicants(temp);
                setShortList(tempShortlist)
                setLoading(false)
            } else {
                setLoading(false)
            }
        }, ()=>{})
    }, [])

    

    const exportAll = async (type) => {
        const jsonArray = []
        switch (type) {
            case "ALL": {
                const fileName = 'Applicants-' + role + "-" + companyName    
                const exportType = 'xls'  
                for (const index in applications) {
                    jsonArray.push({"Student Name":removeComma(applications[index].name.toString()), USN:removeComma(applications[index].usn.toString()), College:removeComma(applications[index].collegeName.toString()), Department:removeComma(applications[index].department.toString()), Semester:removeComma(applications[index].semester.toString()), CGPA:removeComma(applications[index].cgpa.toString()), "10th Percentage":removeComma(applications[index].tenthPercentage.toString()), "12th Percentage":removeComma(applications[index].twelthPercentage.toString()), "Phone Number":removeComma(applications[index].phoneNumber.toString()), "Email-ID":removeComma(applications[index].emailId.toString())});
                }
                exportFromJSON({ data:jsonArray, fileName, exportType })  
                break
            }
            case "ACCEPTED": {
                const fileName = 'Accepted-' + role + "-" + companyName    
                const exportType = 'xls'  
                for (const index in applications) {
                    if (applications[index].status === "Accepted") {
                        jsonArray.push({"Student Name":removeComma(applications[index].name.toString()), USN:removeComma(applications[index].usn.toString()), College:removeComma(applications[index].collegeName.toString()), Department:removeComma(applications[index].department.toString()), Semester:removeComma(applications[index].semester.toString()), CGPA:removeComma(applications[index].cgpa.toString()), "10th Percentage":removeComma(applications[index].tenthPercentage.toString()), "12th Percentage":removeComma(applications[index].twelthPercentage.toString()), "Offer (₹)":removeComma(applications[index].package.toString()), "Phone Number":removeComma(applications[index].phoneNumber.toString()), "Email-ID":removeComma(applications[index].emailId.toString())});
                    }
                }
                exportFromJSON({ data:jsonArray, fileName, exportType })  
                break
            }
            case "REJECTED": {
                const fileName = 'Rejected-' + role + "-" + companyName    
                const exportType = 'xls'  
                for (const index in applications) {
                    if (applications[index].status === "Rejected") {
                        jsonArray.push({"Student Name":removeComma(applications[index].name.toString()), USN:removeComma(applications[index].usn.toString()), College:removeComma(applications[index].collegeName.toString()), Department:removeComma(applications[index].department.toString()), Semester:removeComma(applications[index].semester.toString()), CGPA:removeComma(applications[index].cgpa.toString()), "10th Percentage":removeComma(applications[index].tenthPercentage.toString()), "12th Percentage":removeComma(applications[index].twelthPercentage.toString()), "Phone Number":removeComma(applications[index].phoneNumber.toString()), "Email-ID":removeComma(applications[index].emailId.toString())});
                    }
                }
                exportFromJSON({ data:jsonArray, fileName, exportType })  
                break
            }
            default: {}
        }

        var tempKey = push(ref(firebaseDatabase, "LOG/COLLEGE/")).key
        var date = new Date().getDate(); 
        var month = new Date().getMonth() + 1; 
        var year = new Date().getFullYear(); 
        var hours = new Date().getHours(); 
        var min = new Date().getMinutes();
        var sec = new Date().getSeconds(); 
        var logDate = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
        var tempObj = {key:tempKey, date:logDate, id:globalState.uid, name:globalState.collegeName, url:globalState.companyLogo, type:"EXPORT"}
        await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/log/"+tempKey), tempObj)
        await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/name"), tempObj.name)
    }

    const removeComma = (input) => {
        return input.split(",").join(" ");
    }

    const exportShortListed = async () => {
        console.log(shortList)
        if (shortList.length === 0) {
            alert("No shortlisted students found.")
        } else {
            const jsonArray = []
            const fileName = 'Shortlist-' + role + "-" + companyName    
            const exportType = 'xls'  
            for (const index in shortList) {
                jsonArray.push({"Student Name":shortList[index].name.toString(), USN:shortList[index].usn.toString(), College:shortList[index].collegeName.toString(), Department:shortList[index].department.toString(), Semester:shortList[index].semester.toString(), CGPA:shortList[index].cgpa.toString(), "10th Percentage":applications[index].tenthPercentage.toString(), "12th Percentage":applications[index].twelthPercentage.toString(), "Phone Number":applications[index].phoneNumber.toString(), "Email-ID":applications[index].emailId.toString()});
            }
            exportFromJSON({ data:jsonArray, fileName, exportType }) 
            var tempKey = push(ref(firebaseDatabase, "LOG/COLLEGE/")).key
            var date = new Date().getDate(); 
            var month = new Date().getMonth() + 1; 
            var year = new Date().getFullYear(); 
            var hours = new Date().getHours(); 
            var min = new Date().getMinutes();
            var sec = new Date().getSeconds(); 
            var logDate = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
            var tempObj = {key:tempKey, date:logDate, id:globalState.uid, name:globalState.collegeName, url:globalState.companyLogo, type:"EXPORT"}
            await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/log/"+tempKey), tempObj)
            await set(ref(firebaseDatabase, "LOG/COLLEGE/"+tempObj.id+"/name"), tempObj.name)
        }
    }

    return(
        <div className="applications-page-container">
            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>  
            <div className="list-container">
                <div className="title-selector-container">
                    <p className="section-title">Applications</p>
                    <div style={{ display:'flex' }} className="filter-buttons">
                        {/* <FiltersModal filters={filters} setFilters={setFilters} /> */}
                        <Dropdown>
                            <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                Export Excel
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                <Dropdown.Item onClick={()=>{exportAll("ALL")}}>All Applicants</Dropdown.Item>
                                <Dropdown.Item onClick={exportShortListed}>Shortlisted</Dropdown.Item>
                                <Dropdown.Item onClick={()=>{exportAll("PLACED")}}>Placed</Dropdown.Item>
                                <Dropdown.Item onClick={()=>{exportAll("REJECTED")}}>Rejected</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>

                    </div>
                </div>
                {
                    loading
                    ?
                    <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center"}}  />
                    :
                    applications.length === 0
                    ? 
                    <p style={{marginTop:50, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC"}}>No applications yet!</p>
                    :
                    <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Name</th>
                            <th>Usn</th>
                            <th>College</th>
                            <th>Department</th>
                            <th>Offer</th>
                            <th> </th>
                            <th>Shortlist</th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            applications.map((item, index) => {return(
                                <Applicant item={item} jobId={jobId} companyId={companyId} shortList={shortList} setShortList={setShortList} />
                            )})
                        }
                    </tbody>
                </Table>
                }
            </div>
        </div>
    )
}

export default Applications
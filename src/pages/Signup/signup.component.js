import React from 'react';
import { Helmet } from 'react-helmet';
import logo from '../../assets/logo.png'
import SignupBox from '../../components/signup-box/Signup-box.component';
import './signup.style.scss'

const SignupPage = () => {
    return(
        <div className='signup-container'>
            <Helmet>
                <title>Create Account | OccuHire</title>
            </Helmet>
            <header>
                <img src={logo} alt='OccuHire' width={250} />
            </header>
            <main className='main-container' >
                <div className='main-box' >
                    <h2 className='labels' >Create Account</h2>
                    <SignupBox />
                    <hr className="separator" />
                    <div className='bottom-options' >
                        <span className='labels' >Terms & Condition</span>
                        <span className='labels label-button' ><span className='new-account-label' >Privacy Policy</span></span>
                    </div>
                </div> 
            </main>
        </div>
    )
}

export default SignupPage;
import React, { useState, useEffect } from "react";
import { Form, Table, Button, Spinner, Modal } from "react-bootstrap";
import './student-detail.style.scss'
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer, useToast } from "react-toastify";
import { set, ref, get, child } from 'firebase/database'
import logo from '../../assets/logo.png'
import { useLocation, useNavigate } from "react-router-dom";
import { firebaseDatabase } from "../../backend/firebase-handler";
import { IoMdArrowRoundBack } from 'react-icons/io'

const StudentDetail = () => {

    const location = useLocation()
    const navigate = useNavigate()
    const [userProfile, setUserProfile] = useState(location.state.item)
    const type = location.state.type
    const [contentLoading, setContentLoading] = useState(true)
    const jobId = type==="RESUME"?"":location.state.jobId
    const companyId = type==="RESUME"?"":location.state.companyId
    const disableEditing = type!=="RESUME"
    const [loading, setLoading] = useState(false)
    const [drivesList, setDriveList] = useState([])
    const [modalVisibility, setModalVisibility] = useState(false)
    const [offeredPackage, setPackage] = useState("")
    const [tempName, setTempName] = useState("")
    const [tempDesc, setTempDesc] = useState("")
    const [tempIntComp, setTempComp] = useState("")
    const [tempDuration, setTeampDUration] = useState("")
    const [tempRole, setTempRole] = useState("")
    const [tempDate, setTempDate] = useState("")
    const [tempIntDisc, setTempIntDisc] = useState("")
    const [certName, setCertName] = useState("")
    const [certAgency, setCertAgency] = useState("")
    const [certDate, setCertDate] = useState("")
    const [paperTitle, setPaperTitle] = useState("")
    const [paperAgency, setPaperAgency] = useState("")
    const [paperDate, setPaperDate] = useState("")
    const [paperdisc, setPaperdisc] = useState("")
    const [awardName, setAwardName] = useState("")
    const [awardDate, setAwardDate] = useState("")
    const [awardDisc, setAwardDisc] = useState("")
    const [addDetail, setAddDetail] = useState("")
    const [addName, setAddName] = useState("")

    useEffect(() => {
        setContentLoading(false)

        var temp = []

        get(child(ref(firebaseDatabase), "APPLICATION_STATUS/"+userProfile.key)).then((snap)=>{
            if (snap.exists()) {
                for(const key in snap.val()) {
                    temp.push(snap.child(key).val())
                }
                setDriveList(temp)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleButton = async (item, type) => {
        item.status = type
        setLoading(true)
        await set(ref(firebaseDatabase, "COLLEGE_APPLICANTS_LIST/"+item.collegeUid+"/"+companyId+"/"+jobId+"/"+item.key+"/status"), type)
        await set(ref(firebaseDatabase, "APPLICATION_STATUS/"+item.key+"/"+jobId+"/status"), type)

        if (type === "Placed") {
            setModalVisibility(false)
            await get(child(ref(firebaseDatabase), "STUDENTS_ARCHIVE/"+item.department+"/"+item.key+"/placed")).then(async(snap)=>{
                let array = []
                if(snap.exists()) {
                    array = snap.val()
                    let tempObj = {id:jobId, package:offeredPackage}
                    array.push(tempObj)
                } else {
                    let tempObj = {id:jobId, package:offeredPackage}
                    array.push(tempObj)
                }
                await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+item.department+"/"+item.key+"/placed"), array)
                await set(ref(firebaseDatabase, "APPLICATION_STATUS/"+item.key+"/"+jobId+"/package"), offeredPackage)
            })
            if (userProfile.accountType === "LINKED") {
                await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+item.collegeUid+"/"+item.department+"/"+item.collegeKey+"/placed")).then(async(snap)=>{
                    let array = []
                    if(snap.exists()) {
                        array = snap.val()
                        let tempObj = {id:jobId, package:offeredPackage}
                        array.push(tempObj)
                    } else {
                        let tempObj = {id:jobId, package:offeredPackage}
                        array.push(tempObj)
                    }
                    await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+item.collegeUid+"/"+item.department+"/"+item.collegeKey+"/placed"), array)
                    await set(ref(firebaseDatabase, "COLLEGE_APPLICANTS_LIST/"+item.collegeUid+"/"+companyId+"/"+jobId+"/"+item.key+"/package"), offeredPackage)
                })
            }
        }
        toast.success("Student " + type)
    }

    const handleAdd = (type) => {
        switch(type) {
            case "project": {
                var tempObj = {name:tempName, description:tempDesc}
                var tempArray = userProfile.projectDetails?userProfile.projectDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, projectDetails:tempArray})
                setTempName("")
                setTempDesc("")
                break
            }
            case "internship": {
                let tempObj = {compName:tempIntComp, role:tempRole, duration:tempDuration, date:tempDate, description:tempIntDisc}
                var tempArray = userProfile.internshipDetails?userProfile.internshipDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, internshipDetails:tempArray})
                setTempComp("")
                setTempRole("")
                setTeampDUration("")
                setTempDate("")
                setTempIntDisc("")
                break
            }
            case "certificate": {
                let tempObj = {name:certName, date:certDate, agency:certAgency}
                var tempArray = userProfile.certificationDetails?userProfile.certificationDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, certificationDetails:tempArray})
                setCertName("")
                setCertDate("")
                setCertAgency("")
                break
            }
            case "papers": {
                let tempObj = {title:paperTitle, agency:paperAgency, date:paperDate, description:paperdisc}
                var tempArray = userProfile.researchPapers?userProfile.researchPapers:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, researchPapers:tempArray})
                setPaperTitle("")
                setPaperAgency("")
                setPaperDate("")
                setPaperdisc("")
                break
            }
            case "award": {
                let tempObj = {name:awardName, date:awardDate, description:awardDisc}
                var tempArray = userProfile.awards?userProfile.awards:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, awards:tempArray})
                setAwardName("")
                setAwardDate("")
                setAwardDisc("")
                break
            }
            case "additional": {
                let tempObj = {name:addName, description:addDetail}
                var tempArray = userProfile.additionalDetails?userProfile.additionalDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, additionalDetails:tempArray})
                setAddName("")
                setAddDetail("")
                break
            }
            default: {}
        }
    }

    const handleSave = async (event) => {
        event.preventDefault()

        if (userProfile.phoneNumber.length !== 10) {
            toast.warn("Please enter a valid Phone Number")
            return
        }
        if (userProfile.aadharNumber.length !== 12) {
            toast.warn("Please enter a valid Aadhar Card Number")
            return
        }
        var count = 0
        for (const index in Object.values(userProfile.interestedDomain)) {
            if (Object.values(userProfile.interestedDomain)[index]) {
                count++
            }
        }
        if (count === 0) {
            toast.warn("Please select at least one Interested Domain")
            return
        }
        if (userProfile.proficiency === "") {
            toast.warn("Please select Programming Proficiency")
            return
        }
        if (userProfile.expectedPackage === "") {
            toast.warn("Please select expected package")
            return
        }
        setLoading(true)
        await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+userProfile.collegeUid+"/"+userProfile.department+"/"+userProfile.collegeKey), userProfile)
        if (userProfile.accountType === "LINKED") {
            await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userProfile.department+"/"+userProfile.uid), userProfile)
        }
        toast.success("Student details updated.")
        setLoading(false)
    }

    return(
        <div className="manage-resume-container">
            
            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>      
           {
                contentLoading
                ?
                <div></div>
                :
                type === "OFFERS"
                ?
                <div className="table-container">
                    <Table hover className="table" responsive>
                        <thead>
                            <tr>
                                <th>Company</th>
                                <th>Role</th>
                                <th>Package</th>
                                <th>Location</th>
                                <th>Offer</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody className="table-body">
                            {
                                loading
                                ?
                                <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center"}} />
                                :
                                drivesList.length === 0
                                ?
                                <p style={{marginTop:50, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC", marginBottom:50}}>No offers yet!</p>
                                :
                                drivesList.map((item, index) => {return(
                                    <tr>
                                        <td>
                                            <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                <img src={item.companyLogo?item.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fbuildings.png?alt=media&token=41b80aa2-3348-4616-8b04-5f155d9e0b55"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                <p>{item.companyName}</p>
                                            </div>
                                        </td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.status==="Placed"?"₹ "+item.package:"-"}</div></td>
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" style={item.status==="Placed"?{backgroundColor:"#4BCD7F"}:item.status==="Rejected"?{backgroundColor:"#D43B3B"}:item.status==="Applied"?{backgroundColor:"#52E1E2"}:{backgroundColor:"#FFB800"}} variant="primary">{item.status}</Button></div></td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </Table>
                </div>
                :
                <div className="download-form-container">

                    <Form onSubmit={handleSave} className="resume-form-container">

                    <p className="ete-field section-title">Personal Details</p>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Full Name</Form.Label>
                        <Form.Control required className="field" disabled={disableEditing} type="text" placeholder="FirstName LastName" value={userProfile.name} onChange={(event)=>{setUserProfile({...userProfile, name:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">USN</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.usn} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Phone Number</Form.Label>
                        <Form.Control required maxLength={10} disabled={disableEditing} className="field" type="number-pad" placeholder="+91 xxxxx xxxxx" value={userProfile.phoneNumber} onChange={(event)=>{setUserProfile({...userProfile, phoneNumber:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Email</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.emailId} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">About Yourself</Form.Label>
                        <Form.Control required as="textarea" rows={3} maxLength={400} disabled={disableEditing} className="field" type="text" value={userProfile.aboutYourself} onChange={(event)=>{setUserProfile({...userProfile, aboutYourself:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">College Name</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" placeholder="ABC College" value={userProfile.collegeName} onChange={(event)=>{setUserProfile({...userProfile, collegeName:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Department</Form.Label>
                        <Form.Control required className="field" value={userProfile.department} disabled />             
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Semester</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" placeholder="7th Semester" value={userProfile.semester} onChange={(event)=>{setUserProfile({...userProfile, semester:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Gender</Form.Label>
                        <Form.Select aria-label="Default select example" disabled={disableEditing} value={userProfile.gender} onChange={(event)=>{setUserProfile({...userProfile, gender:event.target.value})}}>
                            <option>Select</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Graduation Year</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" placeholder="3rd Year" value={userProfile.yearOfStudy} onChange={(event)=>{setUserProfile({...userProfile, yearOfStudy:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date of Birth</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="date" placeholder="DD/MM/YYYY" value={userProfile.dateOfBirth} onChange={(event)=>{setUserProfile({...userProfile, dateOfBirth:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Aadhar Number</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" maxLength={12} placeholder="XXXX XXXX XXXX" value={userProfile.aadharNumber} onChange={(event)=>{setUserProfile({...userProfile, aadharNumber:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">LinkedIn Profile URL</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" placeholder="https://www.linkedin.com/in/....." value={userProfile.linkedInUrl} onChange={(event)=>{setUserProfile({...userProfile, linkedInUrl:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Address</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" placeholder="Address, Street, City, State" value={userProfile.address} onChange={(event)=>{setUserProfile({...userProfile, address:event.target.value})}} />
                    </Form.Group>

                    <p className="section-title">Education Details</p>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard School</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" placeholder="ABC School" value={userProfile.tenthSchool} onChange={(event)=>{setUserProfile({...userProfile, tenthSchool:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard Board</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" placeholder="CBSE/ICSE/State/Other" value={userProfile.tenthBoard} onChange={(event)=>{setUserProfile({...userProfile, tenthBoard:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard Percentage</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" placeholder="XX %" value={userProfile.tenthPercentage} onChange={(event)=>{setUserProfile({...userProfile, tenthPercentage:event.target.value.replace(/[^0-9.]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard School</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" placeholder="ABC School" value={userProfile.twelthSchool} onChange={(event)=>{setUserProfile({...userProfile, twelthSchool:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard Board</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" placeholder="CBSE/ICSE/State/Other" value={userProfile.twelthBoard} onChange={(event)=>{setUserProfile({...userProfile, twelthBoard:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard Percentage</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" placeholder="XX %" value={userProfile.twelthPercentage} onChange={(event)=>{setUserProfile({...userProfile, twelthPercentage:event.target.value.replace(/[^0-9.]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Current CGPA</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="text" placeholder="X" value={userProfile.cgpa} onChange={(event)=>{setUserProfile({...userProfile, cgpa:event.target.value.replace(/[^0-9.]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">No. of Active Backlogs</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" type="number-pad" placeholder="X" value={userProfile.backlogs} onChange={(event)=>{setUserProfile({...userProfile, backlogs:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <p className="section-title">Technical Skills Details</p>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Programming languages known (Optional)</Form.Label>
                        <Form.Control disabled={disableEditing} className="field" placeholder="C++, Java" value={userProfile.languagesKnown} onChange={(event)=>{setUserProfile({...userProfile, languagesKnown:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Technologies prevalent in</Form.Label>
                        <Form.Control required disabled={disableEditing} className="field" placeholder="Machine Learning, Data Science" value={userProfile.prevalentTechnologies} onChange={(event)=>{setUserProfile({...userProfile, prevalentTechnologies:event.target.value})}} />
                    </Form.Group>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Add project details (Click "Add" to save project)</p>
                        <p className="add-button" onClick={()=>{handleAdd("project")}}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Project name</Form.Label>
                        <Form.Control className="field" placeholder="XYZ Project" value={tempName} onChange={(event)=>{setTempName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Project description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={tempDesc} onChange={(event)=>{setTempDesc(event.target.value)}} />
                    </Form.Group>
                    {
                        (userProfile.projectDetails && userProfile.projectDetails.length !== 0)
                        &&
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Remove Project</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.projectDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td style={{wordBreak:"break-all"}}>{item.description}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.projectDetails.filter((currentItem) => currentItem !== item)
                                                setUserProfile({...userProfile, projectDetails:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Add internship details (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("internship")}}>Add</p>
                    </div>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Company Name</Form.Label>
                        <Form.Control className="field" type="text" placeholder="ABC Company" value={tempIntComp} onChange={(event)=>{setTempComp(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Role</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Tester" value={tempRole} onChange={(event)=>{setTempRole(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Duration</Form.Label>
                        <Form.Control className="field" type="text" placeholder="6 months" value={tempDuration} onChange={(event)=>{setTeampDUration(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date</Form.Label>
                        <Form.Control className="field" type="date" placeholder="DD/MM/YYYY" value={tempDate} onChange={(event)=>{setTempDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={tempIntDisc} onChange={(event)=>{setTempIntDisc(event.target.value)}} />
                    </Form.Group>

                    {
                        (userProfile.internshipDetails && userProfile.internshipDetails.length !== 0)
                        &&
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Role</th>
                                    <th>Duration</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Description</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.internshipDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{item.compName}</td>
                                            <td>{item.role}</td>
                                            <td>{item.duration}</td>
                                            <td>{item.startDate}</td>
                                            <td>{item.endDate}</td>
                                            <td>{item.description}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.internshipDetails.filter((currentItem) => currentItem !== item)
                                                setUserProfile({...userProfile, internshipDetails:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Add certificate details (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("certificate")}}>Add</p>
                    </div>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Certificate Name</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Appreciation for ..." value={certName} onChange={(event)=>{setCertName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date</Form.Label>
                        <Form.Control className="field" type="date" placeholder="DD/MM/YYYY" value={certDate} onChange={(event)=>{setCertDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Issuing Agency</Form.Label>
                        <Form.Control className="field" placeholder="ABC Company" value={certAgency} onChange={(event)=>{setCertAgency(event.target.value)}} />
                    </Form.Group>

                    {
                        (userProfile.certificationDetails && userProfile.certificationDetails.length !== 0)
                        &&
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Certificate Name</th>
                                    <th>Date</th>
                                    <th>Issuing Agency</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.certificationDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.date}</td>
                                            <td>{item.agency}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.certificationDetails.filter((currentItem) => currentItem !== item)
                                                setUserProfile({...userProfile, certificationDetails:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Research paper publications (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("papers")}}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Title</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Analysis of ..." value={paperTitle} onChange={(event)=>{setPaperTitle(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Publication</Form.Label>
                        <Form.Control className="field" type="text" placeholder="IEEE" value={paperAgency} onChange={(event)=>{setPaperAgency(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date</Form.Label>
                        <Form.Control className="field" type="date" placeholder="DD/MM/YYYY" value={paperDate} onChange={(event)=>{setPaperDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={paperdisc} onChange={(event)=>{setPaperdisc(event.target.value)}} />
                    </Form.Group>

                    {
                        (userProfile.researchPapers && userProfile.researchPapers.length !== 0)
                        &&
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Agency</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.researchPapers.map((item, index)=>{return(
                                        <tr>
                                            <td>{item.title}</td>
                                            <td>{item.agency}</td>
                                            <td>{item.date}</td>
                                            <td>{item.description}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.researchPapers.filter((currentItem) => currentItem !== item)
                                                setUserProfile({...userProfile, researchPapers:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Awards and Achievements (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("award")}}>Add</p>
                    </div>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Name</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Award for ..." value={awardName} onChange={(event)=>{setAwardName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date</Form.Label>
                        <Form.Control type="date" className="field" placeholder="DD/MM/YYYY" value={awardDate} onChange={(event)=>{setAwardDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet" value={awardDisc} onChange={(event)=>{setAwardDisc(event.target.value)}} />
                    </Form.Group>

                    {
                        (userProfile.awards && userProfile.awards.length !== 0)
                        &&
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.awards.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.date}</td>
                                            <td>{item.description}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.awards.filter((currentItem) => currentItem !== item)
                                                setUserProfile({...userProfile, awards:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Additional Details (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("additional")}}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Name</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={addName} onChange={(event)=>{setAddName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={addDetail} onChange={(event)=>{setAddDetail(event.target.value)}} />
                    </Form.Group>

                    {
                        (userProfile.additionalDetails && userProfile.additionalDetails.length !== 0)
                        &&
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.additionalDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.description}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.additionalDetails.filter((currentItem) => currentItem !== item)
                                                setUserProfile({...userProfile, additionalDetails:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }

                    <p className="section-title">More about yourself</p>
                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Programming Proficiency</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I don't know programming"} label={"I don't know programming"} value={"I don't know programming"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} />   
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I have studied programming as a part of curriculum"} label={"I have studied programming as a part of curriculum"} value={"I have studied programming as a part of curriculum"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} /> 
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I practice coding occasionally"} label={"I practice coding occasionally"} value={"I practice coding occasionally"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} /> 
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I am a pro coder"} label={"I am a pro coder"} value={"I am a pro coder"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} /> 
                        </div>
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Expected Salary Package</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="2-4 L CTC"} label={"2-4 L CTC"} value={"2-4 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} />   
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="4-7 L CTC"} label={"4-7 L CTC"} value={"4-7 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="7-10 L CTC"} label={"7-10 L CTC"} value={"7-10 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage===">10 L CTC"} label={">10 L CTC"} value={">10 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="Open for all packages"} label={"Open for all packages"} value={"Open for all packages"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                        </div>
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Interested Domains</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group3" type='checkbox' label={"Developer (C, C++, Java, Python)"} value={"Developer (C, C++, Java, Python"} checked={userProfile.interestedDomain.developer} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, developer:!userProfile.interestedDomain.developer}})}} />   
                            <Form.Check inline name="group3" type='checkbox' label={"Software Testing"} value={"Software Testing"} checked={userProfile.interestedDomain.testing} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, testing:!userProfile.interestedDomain.testing}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Analyst"} value={"Analyst"} checked={userProfile.interestedDomain.analyst} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, analyst:!userProfile.interestedDomain.analyst}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Data Scientist"} value={"Data Scientist"} checked={userProfile.interestedDomain.dataScientist} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, dataScientist:!userProfile.interestedDomain.dataScientist}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Dev-Ops Engineer"} value={"Dev-Ops Engineer"} checked={userProfile.interestedDomain.devOps} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, devOps:!userProfile.interestedDomain.devOps}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Full Stack Web Developer"} value={"Full Stack Web Developer"} checked={userProfile.interestedDomain.fullStackWeb} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, fullStackWeb:!userProfile.interestedDomain.fullStackWeb}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Marketing Roles"} value={"Marketing Roles"} checked={userProfile.interestedDomain.marketing} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, marketing:!userProfile.interestedDomain.marketing}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Business Development"} value={"Business Development"} checked={userProfile.interestedDomain.businessDev} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, businessDev:!userProfile.interestedDomain.businessDev}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"I am not sure. Open for any role."} value={"I am not sure. Open for any role."} checked={userProfile.interestedDomain.notsure} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, notsure:!userProfile.interestedDomain.notsure, developer:false, testing:false, analyst:false, dataScientist:false, devOps:false, fullStackWeb:false, marketing:false, businessDev:false}})}} /> 
                        </div>
                    </Form.Group>
                    {
                        type === "RESUME"
                        &&
                        <Button type="submit" className="ete-field submit-button" disabled={loading} variant="primary">Save changes</Button>
                    }
                </Form>
                {
                    type !== "RESUME"
                    &&
                    <div style={{display:"flex", flexDirection:"row", alignItems:"center", justifyContent:"center", width:"100%"}}>
                        <Button className="upgrade-button-secondary" disabled={loading} variant="primary" onClick={()=>{handleButton(userProfile, "Rejected")}}>Reject</Button>
                        <Button className="upgrade-button" disabled={loading} variant="primary" onClick={()=>{setModalVisibility(true)}}>Accept</Button>
                    </div>
                }
                        
            </div>
            }
            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />
            <Modal show={modalVisibility} onHide={()=>{setModalVisibility(false)}}>
                <Modal.Header closeButton >
                  <Modal.Title>Offered package</Modal.Title>
                </Modal.Header>
                                
                <Modal.Body>
                  <p>Please enter the offered package amount.</p>
                  <input placeholder="₹ X,XX,XXX" value={offeredPackage} onChange={(event)=>{setPackage(event.target.value)}} />
                </Modal.Body>
                                
                <Modal.Footer>
                  <Button variant="secondary" onClick={()=>{setModalVisibility(false)}}>Close</Button>
                  <Button variant="primary" onClick={()=>{handleButton(userProfile, "Placed")}}>Submit</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default StudentDetail
import React, {useContext, useEffect, useState} from 'react';
import { firebaseAuth, firebaseDatabase } from '../../backend/firebase-handler';
import './home.style.scss';
import { Image, Spinner } from 'react-bootstrap';
import logo from '../../assets/logo.png';
import { Helmet } from 'react-helmet';
import SidebarNavigation from '../../components/sidebar-navigation/sidebar-navigation.component';
import { useNavigate } from 'react-router-dom';
import CollapsableNavigation from '../../components/collapsable-navigation/collapsable-navigation.component';
import HomeContentContainer from '../../components/home-content-container/home-content-container.component';
import AddPostContentContainer from '../../components/add-post-content-container/AddPostContentContainer.component';
import CompanyProfileContainer from '../../components/company-profile-container/CompanyProfileContainer.component';
import ManageStudents from "../../components/manage-students/ManageStudents.component";
import PlacementStats from "../../components/placement-stats/PlacementStats.component";
import ManageCompanies from "../../components/manage-companies/ManageCompanies.component";
import Context from "../../context/appContext";
import ManageJobs from '../../components/manage-jobs/manage-jobs.component';
import Report from '../../components/report/report-component';
import { child, get, ref } from 'firebase/database';
import AssessmentResults from '../../components/assessment-results/assessment-results.component';

const HomePage  = () => {
    let firebaseUser = firebaseAuth.currentUser;
    const navigate = useNavigate();
    const [selectedTab, setSelectedTab] = useState("Dashboard");
    const [loading, setLoading] = useState(true);
    const [globalState, dispatchForGlobalState] = useContext(Context);

    useEffect(() => {

        window.scrollTo(0,0);
        // get(child(ref(firebaseDatabase), "COLLEGE_ARCHIVE/"+firebaseUser.uid)).then((snap)=>{
        //     if (!snap.exists()) {
        //         firebaseAuth.signOut()
        //     }
        // })
        firebaseAuth.onAuthStateChanged(userCallback);
        return () => {
            firebaseAuth.onAuthStateChanged(() => {});
        }
    },[])

    const userCallback = (user) => {
        if (user){
            dispatchForGlobalState({type:'GET_GLOBAL_USER_DATA'});
            setLoading(false)
        }else {
            navigate('/login')
            setLoading(false);
        }
    }

    const handleChange = (option) => {
        setSelectedTab(option);
    }

    if (loading) {
        return (
            <div className="loader-container">
                <Spinner animation="border" />
            </div>
        )
    }

    
    return(
        <div className='home-container'>
            <Helmet>
                {
                    firebaseUser
                    ?
                    <title>{ firebaseUser.displayName?`${firebaseUser.displayName} | `:"" }OccuHire </title>
                    :
                    <title>OccuHire</title>
                }
                
            </Helmet>
            
            <div className='sidebar'>
                <Image className='logo' src={logo} alt='OccuHire' />
                <SidebarNavigation onChange={handleChange} />
            </div>

            
            <div className='content'>
                <div className='collapsable-sidebar-wrapper'>
                    <CollapsableNavigation  onChange={handleChange} />
                </div>
                
                {
                    selectedTab === "Dashboard"
                    ?
                    <HomeContentContainer />
                    :
                    null
                }

                {
                    selectedTab === 'ManageStudents'
                    ?
                    <ManageStudents />
                    :
                    null
                }

                {
                    selectedTab === 'PlacementStats'
                    ?
                    <PlacementStats />
                    :
                    null
                }

                {
                    selectedTab === "ManageJobs"
                    ?
                    <ManageJobs />
                    :
                    null
                }

                {
                    selectedTab === "AddNewJob"
                    ?
                    <ManageCompanies type="SELECT" />
                    :
                    null
                }

                {
                    selectedTab === 'ManageCompanies'
                    ?
                    <ManageCompanies type="VIEW" />
                    :
                    null
                }

                {
                    selectedTab === 'AssessmentResults'
                    ?
                    <AssessmentResults />
                    :
                    null
                }

                {
                    selectedTab === 'GenerateReports'
                    ?
                    <Report />
                    :
                    null
                }

                {
                    selectedTab === 'ManageProfile'
                    ?
                    <CompanyProfileContainer />
                    :
                    null
                }

            </div>
        </div>
    )
}

export default HomePage
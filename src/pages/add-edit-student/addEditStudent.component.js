import React, {useContext, useState, useEffect} from 'react';
import './addEditStudent.style.scss';
import ActionBarComponent from "../../components/action-bar-container/ActionBarContainer.component";
import { Form, Button, Spinner } from "react-bootstrap";
import departmentList from "../../entities/departmentList";
import Context from "../../context/appContext";
import sampleStudent from "../../entities/sampleStudent";
import {firebaseAuth, firebaseDatabase} from "../../backend/firebase-handler";
import {ref, push, set, get} from "firebase/database";
import { ToastContainer, toast } from 'react-toastify';
import {dateTimeString} from "../../entities/dateTimeHandler";
import {useNavigate, useParams} from "react-router-dom";
import logo from '../../assets/logo.png'
import { IoMdArrowRoundBack } from 'react-icons/io'

const AddEditStudent = () => {
    const [studentDetail, setStudentDetail] = useState({...sampleStudent});
    const [globalState, dispatchForGlobalState] = useContext(Context);
    const firebaseUser = firebaseAuth.currentUser;
    const params = useParams();
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (params.studentId === 'add-new-student'){
            setStudentDetail({...sampleStudent});
        }else {
           setLoading(true);
           const existingStudentRef = ref(firebaseDatabase, `COLLEGE_WISE_STUDENTS/${firebaseUser.uid}/${params.department}/${params.studentId}`);
           get(existingStudentRef).then( async (snapshot) => {
               if (snapshot.exists()){
                    const studentDetail = await snapshot.val();
                    setStudentDetail(studentDetail);
               }else {
                   navigate('/student-details/add-new-student')
               }
               setLoading(false);
           })
        }
    }, [])

    const handleChange = (event) => {
        const { name, value } = event.target;
        setStudentDetail(state => {
            return {...state, [name]: value}
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (!studentDetail.department){
            toast.error('Please select department', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return;
        }

        const collegeStudentRef = ref(firebaseDatabase, `COLLEGE_WISE_STUDENTS/${firebaseUser.uid}/${studentDetail.department}`);
        const key = push(collegeStudentRef).key;
        try {
            setLoading(true);
            const finalObject = {...studentDetail, uid:key, collegeName: globalState.collegeName, date: dateTimeString, collegeKey:key, collegeUid:globalState.uid, plan:"Free Plan", accountType:"DIRECT", key:key}
            const newStudentRef = ref(firebaseDatabase, `COLLEGE_WISE_STUDENTS/${firebaseUser.uid}/${studentDetail.department}/${key}`);
            await set(newStudentRef, finalObject);
            setStudentDetail({...sampleStudent})
            toast.success('Student details saved', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }catch (err) {
            toast.error('Something went wrong', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        setLoading(false);
    }

    return (
        <div className='add-edit-student-container'>
            
            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>  
            
            <div className={'add-edit-student-content'} >
                <p className={'section-title'}>Student Details</p>
                <Form onSubmit={handleSubmit} className={'form-content'}>
                    <Form.Group className="mb-3 first-half" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Name</Form.Label>
                        <Form.Control required onChange={handleChange} name={'name'} value={studentDetail.name} className={'text-input'} type="text" placeholder="Student name" />
                    </Form.Group>
                    <Form.Group className="mb-3 second-half" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Phone Number</Form.Label>
                        <Form.Control required onChange={handleChange} name={'phoneNumber'} maxlength={'10'} value={studentDetail.phoneNumber} className={'text-input'} type="tel" placeholder="10 digit phone number" />
                    </Form.Group>
                    <Form.Group className="mb-3 first-half" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Email Id</Form.Label>
                        <Form.Control required onChange={handleChange} name={'emailId'} value={studentDetail.emailId} className={'text-input'} type="email" placeholder="name@example.com" />
                    </Form.Group>
                    <Form.Group className="mb-3 second-half" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>USN</Form.Label>
                        <Form.Control required onChange={handleChange} name={'usn'} value={studentDetail.usn} className={'text-input'}   />
                    </Form.Group>
                    <Form.Group className="mb-3 end-to-end" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Department</Form.Label>
                        <Form.Select onChange={handleChange} name={'department'} value={studentDetail.department} className={'text-input'} aria-label="Default select example">
                            <option>Select department</option>
                            {
                                departmentList.map(item => <option key={item} >{item}</option>)
                            }
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3 first-half" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Graduation Year</Form.Label>
                        <Form.Control required onChange={handleChange} name={'yearOfStudy'} value={studentDetail.yearOfStudy} className={'text-input'} type="number" />
                    </Form.Group>
                    <Form.Group className="mb-3 second-half" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Semester</Form.Label>
                        <Form.Control required onChange={handleChange} name={'semester'} value={studentDetail.semester} className={'text-input'} type="number" />
                    </Form.Group>
                    <Button disabled={loading} variant="primary" className={'end-to-end submit-button'} type="submit">
                        {
                            loading
                            ?
                                <Spinner size={'sm'} animation="border" />
                                :
                                <span>Save Details</span>
                        }
                    </Button>

                </Form>
            </div>
            <ToastContainer/>
        </div>
    )
}

export default AddEditStudent;
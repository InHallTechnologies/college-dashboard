import React, { useContext, useEffect, useState } from 'react';
import ActionBarComponent from '../../components/action-bar-container/ActionBarContainer.component';
import { Form, Button, Spinner, Modal } from 'react-bootstrap';
import './edit-job.style.scss';
import sampleJobs from '../../entities/sampleJobs';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { firebaseAuth } from '../../backend/firebase-handler';
import { firebaseDatabase } from '../../backend/firebase-handler';
import { ref, get, set, push, child, remove } from 'firebase/database';
import { ToastContainer, toast } from 'react-toastify';
import logo from '../../assets/logo.png'
import { Helmet } from 'react-helmet';
import Context from '../../context/appContext';
import Multiselect from 'multiselect-react-dropdown'
import { MdOutlineDeleteOutline } from 'react-icons/md'
import { IoMdArrowRoundBack } from 'react-icons/io'
 
const EditJobPage = (props) => {
    
    const location = useLocation()
    const navigate = useNavigate()
    const type = location.state.type
    const company= type==="EDIT"?null:location.state.company
    const [jobDetail, setJobDetail] = useState(type==="EDIT"?location.state.job:{...sampleJobs})
    const [loading, setLoading] = useState(false);
    const [globalState, dispatchForGlobalState] = useContext(Context)
    const [yearList, setYearList] = useState([])
    const [list, setList] = useState([])
    const [modalVisibility, setModalVisibility] = useState(false)
    const [examList, setExamList] = useState([])
    
    useEffect(() => {     
        let array = []
        get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+globalState.uid)).then((snap)=>{
            if(snap.exists()) {
                for (const key in snap.val()) {
                    let temp = {name:"", id:""}
                    temp.name = key
                    temp.id = key
                    array.push(temp)
                }
                setList(array)
            }
        })
        let yearArray = []
        const year = new Date().getFullYear()
        for (let i=year-5 ; i<=year+5 ; i++) {
            let temp = {name:"", id:""}
            temp.name = i.toString()
            temp.id = i.toString()
            yearArray.push(temp)
        }
        setYearList(yearArray)  
        
        let tempExams = []
        get(child(ref(firebaseDatabase), "PREQUALIFYING_EXAMS")).then((snap) => {
            if (snap.exists()) {
                for (const key in snap.val()) {
                    let tempObj = {name:"", id:""}
                    tempObj.name = snap.child(key).child("courseName").val()
                    tempObj.id = key
                    tempExams.push(tempObj)
                }
                setExamList(tempExams)
            }
        })
    }, [])

    const handleCLick = async (event) => {
        event.preventDefault()
        if (jobDetail.startPackage === "") {
            toast.warn("Please enter Start Package")
            setLoading(false)
            return
        }
        if (jobDetail.endPackage === "") {
            toast.warn("Please enter End Package")
            setLoading(false)
            return
        }
        if (jobDetail.serviceAgreement && jobDetail.numberOfYears==="") {
            toast.warn("Please enter the number of Service Years")
            setLoading(false)
            return
        }
        if (!jobDetail.allBranches && jobDetail.allowedBranches.length === 0) {
            toast.warn("Please select at least one Allowed Branch")
            setLoading(false)
            return
        }
        if (jobDetail.graduationYear.length === 0) {
            toast.warn("Please select allowed years of graduation")
            setLoading(false)
            return
        }
        if (type === "ADD") {
            var date = new Date().getDate(); 
            var month = new Date().getMonth() + 1; 
            var year = new Date().getFullYear(); 
            var hours = new Date().getHours(); 
            var min = new Date().getMinutes();
            var sec = new Date().getSeconds(); 
            jobDetail.postingDate = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
            jobDetail.companyName = company.companyName
            jobDetail.companyUID = company.uid
            jobDetail.companyLogo = company.companyLogo
            jobDetail.collegeId = globalState.uid
            jobDetail.collegeName = globalState.collegeName
            jobDetail.jobId = push(ref(firebaseDatabase, "COLLEGE_VACANCY/"+globalState.uid)).key
            await set(ref(firebaseDatabase, "COLLEGE_VACANCY/"+globalState.uid+"/"+jobDetail.jobId), jobDetail)
            setLoading(false)
            toast.success("Job added.")
        } else {
            await set(ref(firebaseDatabase, "COLLEGE_VACANCY/"+globalState.uid+"/"+jobDetail.jobId), jobDetail)
            setLoading(false)
            toast.success("Job updated.")
        }
    }

    const handleDelete = async () => {
        await remove(ref(firebaseDatabase, "COLLEGE_VACANCY/"+globalState.uid+"/"+jobDetail.jobId))
        toast.success("Vacancy deleted permanently.")
        const timer = setTimeout(() => {
            navigate(-1)
        }, 1500);
        return () => clearTimeout(timer);
    }

    return(
        <div className="job-detail-container">

            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div> 
            
            <Form onSubmit={handleCLick} className="job-detail-container">
                {/* <p className="section-title">{type==="ADD"?company.companyName:jobDetail.companyName}</p> */}
                <div className='ete-field title-delete-container'>
                    <p className="section-title">{type==="ADD"?company.companyName:jobDetail.companyName}</p>
                    {
                        type !== "ADD"
                        &&
                        <div className='delete-container' onClick={()=>{setModalVisibility(true)}}>
                            <MdOutlineDeleteOutline color="#3761EE" size={20} />
                            <p className='delete-tag'>Delete vacancy</p>
                        </div>
                    }
                </div>

                <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Role</Form.Label>
                    <Form.Control className="field" required value={jobDetail.role} onChange={(event)=>{setJobDetail({...jobDetail, role:event.target.value})}} />
                </Form.Group>

                <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Location</Form.Label>
                    <Form.Control className="field" required value={jobDetail.location} onChange={(event)=>{setJobDetail({...jobDetail, location:event.target.value})}} />
                </Form.Group>

                <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Description</Form.Label>
                    <Form.Control className="field" required value={jobDetail.jobDescription} onChange={(event)=>{setJobDetail({...jobDetail, jobDescription:event.target.value})}} />
                </Form.Group>

                <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Start Package (INR)</Form.Label>
                    <Form.Control className="field" required value={jobDetail.startPackage + " LPA"} onChange={(event)=>{setJobDetail({...jobDetail, startPackage:event.target.value.replace(/[^0-9.]/g, '')})}} />
                </Form.Group>

                <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">End Package (INR)</Form.Label>
                    <Form.Control className="field" required value={jobDetail.endPackage + " LPA"} onChange={(event)=>{setJobDetail({...jobDetail, endPackage:event.target.value.replace(/[^0-9.]/g, '')})}} />
                </Form.Group>

                <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Total Openings</Form.Label>
                    <Form.Control className="field" required value={jobDetail.totalOpenings} onChange={(event)=>{setJobDetail({...jobDetail, totalOpenings:event.target.value.replace(/[^0-9]/g, '')})}} />
                </Form.Group>

                <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Cut-off CGPA</Form.Label>
                    <Form.Control className="field" required value={jobDetail.cutoffCGPA} onChange={(event)=>{setJobDetail({...jobDetail, cutoffCGPA:event.target.value.replace(/[^0-9.]/g, '')})}} />
                </Form.Group>

                <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Minimum 10th Percentage</Form.Label>
                    <Form.Control className="field" required value={jobDetail.min10} onChange={(event)=>{setJobDetail({...jobDetail, min10:event.target.value.replace(/[^0-9.]/g, '')})}} />
                </Form.Group>

                <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Minimum 12th Percentage</Form.Label>
                    <Form.Control className="field" required value={jobDetail.min12} onChange={(event)=>{setJobDetail({...jobDetail, min12:event.target.value.replace(/[^0-9.]/g, '')})}} />
                </Form.Group>

                <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Select allowed branches</Form.Label>
                    {
                        !jobDetail.allBranches
                        &&
                        <Multiselect className='field' options={list} selectedValues={jobDetail.allowedBranches} displayValue="name" onSelect={(value)=>{setJobDetail({...jobDetail, allowedBranches:value})}} onRemove={(value)=>{setJobDetail({...jobDetail, allowedBranches:value})}}/>
                    }
                    <div className='ete-field'><Form.Check className="tag" style={{marginTop:5}}  inline label="Allow all branches" name="group1" type={"checkbox"} checked={jobDetail.allBranches} id={"allowAll"} onChange={(event)=>{setJobDetail({...jobDetail, allBranches:!jobDetail.allBranches})}} /></div>
                </Form.Group>

                <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Preferred Skills</Form.Label>
                    <Form.Control className="field" required value={jobDetail.preferredSkills} onChange={(event)=>{setJobDetail({...jobDetail, preferredSkills:event.target.value})}} />
                </Form.Group>

                <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Select year of graduation</Form.Label>
                    <Multiselect className='field' options={yearList} selectedValues={jobDetail.graduationYear} displayValue="name" onSelect={(value)=>{setJobDetail({...jobDetail, graduationYear:value})}} onRemove={(value)=>{setJobDetail({...jobDetail, graduationYear:value})}}/>
                </Form.Group>

                <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Registration Link (Optional)</Form.Label>
                    <Form.Control className="field" value={jobDetail.registrationLink}  onChange={(event)=>{setJobDetail({...jobDetail, registrationLink:event.target.value})}} />
                </Form.Group>

                <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Check className="tag" style={{marginBottom:5}} inline label="Service Agreement" name="group1" type={"checkbox"} id={"serviceAgreement"} checked={jobDetail.serviceAgreement} onChange={(event)=>{setJobDetail({...jobDetail, serviceAgreement:!jobDetail.serviceAgreement})}} />
                    <Form.Control className="field" placeholder="Number of Service Years" value={jobDetail.numberOfYears} disabled={!jobDetail.serviceAgreement} onChange={(event)=>{setJobDetail({...jobDetail, numberOfYears:event.target.value.replace(/[^0-9]/g, '')})}} />
                </Form.Group>

                <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Last Date to Apply</Form.Label>
                    <Form.Control className="field" required value={jobDetail.lastDateToApply} type="date" onChange={(event)=>{setJobDetail({...jobDetail, lastDateToApply:event.target.value})}} />
                </Form.Group>

                <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                    <Form.Label className="tag">Pre Qualifying Exams</Form.Label>
                    <Multiselect className='field' options={examList} selectedValues={jobDetail.preQualifyingExams} displayValue="name" onSelect={(value)=>{setJobDetail({...jobDetail, preQualifyingExams:value})}} onRemove={(value)=>{setJobDetail({...jobDetail, preQualifyingExams:value})}}/>
                </Form.Group>
                
                <Button type="submit" variant="primary" disabled={loading} className="ete-field button" >Save</Button>
               
            </Form>

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />

            <Modal show={modalVisibility} aria-labelledby="contained-modal-title-vcenter" centered >
                <Modal.Header onHide={()=>{setModalVisibility(false)}}>
                    <Modal.Title id="contained-modal-title-vcenter">Confirm!</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Are you sure you want to permanently delete this vacancy?</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={handleDelete}>Delete</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default EditJobPage;